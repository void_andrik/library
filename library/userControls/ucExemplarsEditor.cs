﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class ucExemplarsEditor : UserControl
    {
        public List<exemplarItem> items = new List<exemplarItem>();
        public int _book_id = -1;
        public int exmplr_id = -1;

        public ucExemplarsEditor()
        {
            InitializeComponent();

        }

        public void fill()
        {
            lbList.Items.Clear();
            foreach (exemplarItem mi in items)
            {
                lbList.Items.Add(mi);
            }
        }

        public void load()
        {
            items.Clear();
            librDataSetTableAdapters.exemplarsTableAdapter adapter = new librDataSetTableAdapters.exemplarsTableAdapter();
            librDataSet.exemplarsDataTable table = new librDataSet.exemplarsDataTable();
            adapter.FillBy(table, this._book_id);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                int exmplr_id = (int)table.Rows[i]["exmplr_id"];
                int room = (int)table.Rows[i]["room"];
                int rack = (int)table.Rows[i]["rack"];
                int shelf = (int)table.Rows[i]["shelf"];
                DateTime? date_in = (DateTime?)table.Rows[i]["date_in"];
                DateTime? date_out;
                if (table.Rows[i]["date_out"].ToString() == "")
                    date_out = null;
                else
                    date_out = (DateTime?)table.Rows[i]["date_out"];
                int price = (int)table.Rows[i]["price"];
                string dlv_id = table.Rows[i]["dlv_id"].ToString();
                items.Add(new exemplarItem(exmplr_id, room, rack, shelf, date_in, date_out, price,dlv_id));
            }
            fill();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "exmplr_id", "INT" }, { "room", "INT" }, { "rack", "INT" }, { "shelf", "INT" }, { "date_in", "DAT" }, { "date_out", "NULL_DAT" }, { "price", "INT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "exmplr_id", "Инв. номер" }, { "room", "Комната" }, { "rack", "Шкаф" }, { "shelf", "Полка" }, { "date_in", "Дата приобретения" }, { "date_out", "Дата списания" }, { "price", "Цена" } };
            Dictionary<string, string> v = new Dictionary<string, string>();
            librDataSetTableAdapters.exemplarsTableAdapter ad = new librDataSetTableAdapters.exemplarsTableAdapter();
            int? new_id = ad.getID();
            if (new_id == null)
                new_id = 1;
            v.Add("exmplr_id", new_id.ToString());
            frmRowEditor re = new frmRowEditor(i, d, v);
            re.Text = "Добавление экземпляра";
            if (re.ShowDialog() == DialogResult.OK)
            {
                int exmplr_id = int.Parse(re.getValue("exmplr_id"));
                int cnt = (int)ad.hasID(exmplr_id);
                if (cnt == 0)
                {
                    int room = int.Parse(re.getValue("room"));
                    int rack = int.Parse(re.getValue("rack"));
                    int shelf = int.Parse(re.getValue("shelf"));
                    int price = (int)Math.Truncate((float.Parse(re.getValue("price"))));
                    DateTime? date_in = DateTime.Parse(re.getValue("date_in"));
                    DateTime? date_out = null;
                    if (re.getValue("date_out") != "")
                        date_out = DateTime.Parse(re.getValue("date_out"));
                    try
                    {
                        if (_book_id != -1)
                        {
                            librDataSetTableAdapters.exemplarsTableAdapter adapter = new librDataSetTableAdapters.exemplarsTableAdapter();
                            adapter.Insert(exmplr_id, _book_id, room, rack, shelf, date_in, date_out, price);
                        }
                        items.Add(new exemplarItem(exmplr_id, room, rack, shelf, date_in, date_out, price, ""));
                        fill();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lbList.SelectedItems.Count > 0)
            {
                if (_book_id != -1)
                {
                    exemplarItem mi = (exemplarItem)lbList.SelectedItem;
                    if (mi._exmplr_id != null)
                    {
                        librDataSetTableAdapters.deliveriesTableAdapter adapter0 = new librDataSetTableAdapters.deliveriesTableAdapter();
                        adapter0.DeleteByExmplrID((int)mi._exmplr_id);
                        librDataSetTableAdapters.exemplarsTableAdapter adapter = new librDataSetTableAdapters.exemplarsTableAdapter();
                        adapter.Delete((int)mi._exmplr_id);
                    }
                }
                items.Remove((exemplarItem)lbList.SelectedItem);
                fill();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lbList.SelectedItems.Count == 1)
            {
                exemplarItem itm = (exemplarItem)lbList.SelectedItem;
                librDataSetTableAdapters.exemplarsTableAdapter adapter = new librDataSetTableAdapters.exemplarsTableAdapter();
                int? exmplr_id = itm._exmplr_id;
                Dictionary<string, string> i = new Dictionary<string, string> { { "exmplr_id", "ID" }, { "room", "INT" }, { "rack", "INT" }, { "shelf", "INT" }, { "date_in", "DAT" }, { "date_out", "NULL_DAT" }, { "price", "INT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "exmplr_id", "Инв. номер" }, { "room", "Комната" }, { "rack", "Шкаф" }, { "shelf", "Полка" }, { "date_in", "Дата приобретения" }, { "date_out", "Дата списания" }, { "price", "Цена" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("exmplr_id", exmplr_id.ToString());
                v.Add("room", itm._room.ToString());
                v.Add("rack", itm._rack.ToString());
                v.Add("shelf", itm._shelf.ToString());
                v.Add("date_in", itm._date_in.ToString());
                v.Add("date_out", itm._date_out.ToString());
                v.Add("price", itm._price.ToString());

                frmRowEditor re = new frmRowEditor(i, d, v);
                re.Text = "Редактирование экземпляра";
                if (re.ShowDialog() == DialogResult.OK)
                {
                    itm._room = int.Parse(re.getValue("room"));
                    itm._rack = int.Parse(re.getValue("rack"));
                    itm._shelf = int.Parse(re.getValue("shelf"));
                    itm._price = (int)Math.Truncate((float.Parse(re.getValue("price"))));
                    itm._date_in = DateTime.Parse(re.getValue("date_in"));
                    itm._date_out = null;
                    if (re.getValue("date_out") != "")
                        itm._date_out = DateTime.Parse(re.getValue("date_out"));
                    if (_book_id != -1)
                    {
                        int id = (int)itm._exmplr_id;
                        adapter.Update(_book_id, itm._room, itm._rack, itm._shelf, itm._date_in, itm._date_out, itm._price, id);
                    }
                }
            }
        }

        private void lbList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbList.SelectedItems.Count > 0 && (lbList.SelectedItem as exemplarItem)._dlv_id == "")
                exmplr_id = (int)(lbList.SelectedItem as exemplarItem)._exmplr_id;
            else
                exmplr_id = -1;
        }
    }

    public class exemplarItem
    {
        public int? _exmplr_id;
        public int _room;
        public int _rack;
        public int _shelf;
        public DateTime? _date_in;
        public DateTime? _date_out;
        public int _price;
        public string _dlv_id;

        public exemplarItem(int? exmplr_id,int room, int rack, int shelf, DateTime? date_in, DateTime? date_out, int price, string dlv_id)
        {
            this._exmplr_id = exmplr_id;
            this._room = room;
            this._rack = rack;
            this._shelf = shelf;
            this._date_in = date_in;
            this._date_out = date_out;
            this._price = price;
            this._dlv_id = dlv_id;
        }

        public override string ToString()
        {
            string isOut = _dlv_id == "" ? "" : "на руках: ";
            return string.Format("{0}Инв. №{1} (к:{2}, ш:{3}, п:{4})",isOut, this._exmplr_id, this._room, this._rack, this._shelf);
        }
    }
}
