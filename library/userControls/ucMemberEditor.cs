﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class ucMemberEditor : UserControl
    {
        public List<memberItem> items = new List<memberItem>();
        public int _book_id = -1;

        public ucMemberEditor()
        {
            InitializeComponent();
        }

        public void fill()
        {
            lbList.Items.Clear();
            foreach (memberItem mi in items)
            {
                lbList.Items.Add(mi);
            }
        }

        public void load()
        {
            items.Clear();
            librDataSetTableAdapters.membersTableAdapter adapter = new librDataSetTableAdapters.membersTableAdapter();
            librDataSet.membersDataTable table = new librDataSet.membersDataTable();
            adapter.FillBy(table, this._book_id);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                int mbr_id = (int)table.Rows[i]["mbr_id"];
                int mtyp_id = (int)table.Rows[i]["mtyp_mtyp_id"];
                int ppl_id = (int)table.Rows[i]["ppl_ppl_id"];
                items.Add(new memberItem(mbr_id, mtyp_id, ppl_id));
            }
            fill();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "mtyp_id", "PCK" }, {"ppl_id","PCK"} };
            Dictionary<string, string> d = new Dictionary<string, string> { { "mtyp_id", "Тип участника" }, { "ppl_id", "Имя" } };
            frmRowEditor re = new frmRowEditor(i, d);
            re.Text = "Добавление участника";
            if (re.ShowDialog() == DialogResult.OK)
            {
                int? mbr_id = null;
                int mtyp_id = re.getValueID("mtyp_id");
                int ppl_id = re.getValueID("ppl_id");
                if (mtyp_id != -1 && ppl_id != -1)
                {
                    if (_book_id != -1)
                    {
                        librDataSetTableAdapters.membersTableAdapter adapter = new librDataSetTableAdapters.membersTableAdapter();
                        mbr_id = adapter.InsertAndGetID(_book_id, ppl_id, mtyp_id);
                    }

                    items.Add(new memberItem(mbr_id, mtyp_id, ppl_id));
                    fill();
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (lbList.SelectedItems.Count > 0)
            {
                if (_book_id != -1)
                {
                    memberItem mi = (memberItem)lbList.SelectedItem;
                    if (mi._mbr_id != null)
                    {
                        librDataSetTableAdapters.membersTableAdapter adapter = new librDataSetTableAdapters.membersTableAdapter();
                        adapter.Delete((int)mi._mbr_id);
                    }
                }
                items.Remove((memberItem)lbList.SelectedItem);
                fill();
            }
        }

    }

    public class memberItem
    {
        public int? _mbr_id;
        public int _mtyp_mtyp_id;
        public int _ppl_ppl_id;

        public memberItem(int? mbr_id, int mtyp_id, int ppl_id)
        {
            this._mbr_id = mbr_id;
            this._mtyp_mtyp_id = mtyp_id;
            this._ppl_ppl_id = ppl_id;
        }

        public override string ToString()
        {
            librDataSetTableAdapters.member_typesTableAdapter mt = new librDataSetTableAdapters.member_typesTableAdapter();
            librDataSetTableAdapters.peoples_membersTableAdapter ma = new librDataSetTableAdapters.peoples_membersTableAdapter();
            string mtyp = mt.getDef(_mtyp_mtyp_id);
            string ppl = ma.getName(_ppl_ppl_id);
            return string.Format("{0}:{1}", mtyp, ppl);
        }
    }
}
