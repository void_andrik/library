﻿namespace library
{
    partial class frmEditBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditBook));
            this.lblTitle = new System.Windows.Forms.Label();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbPublisher = new System.Windows.Forms.TextBox();
            this.lblPublisher = new System.Windows.Forms.Label();
            this.tbISBN = new System.Windows.Forms.TextBox();
            this.lblISBN = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblPages = new System.Windows.Forms.Label();
            this.tbBBK = new System.Windows.Forms.TextBox();
            this.lblBBK = new System.Windows.Forms.Label();
            this.tbA_Znak = new System.Windows.Forms.TextBox();
            this.lblA_Znak = new System.Windows.Forms.Label();
            this.lblVol = new System.Windows.Forms.Label();
            this.lblVer = new System.Windows.Forms.Label();
            this.tbTyp = new System.Windows.Forms.TextBox();
            this.lblTyp = new System.Windows.Forms.Label();
            this.tbAnnotation = new System.Windows.Forms.TextBox();
            this.lblAnnotation = new System.Windows.Forms.Label();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.numPages = new System.Windows.Forms.NumericUpDown();
            this.numVol = new System.Windows.Forms.NumericUpDown();
            this.numVer = new System.Windows.Forms.NumericUpDown();
            this.btnPublisher = new System.Windows.Forms.Button();
            this.btnTyp = new System.Windows.Forms.Button();
            this.btnOut = new System.Windows.Forms.Button();
            this.ucMemberEditor1 = new library.ucMemberEditor();
            this.label1 = new System.Windows.Forms.Label();
            this.ucExemplarsEditor1 = new library.ucExemplarsEditor();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVer)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 461);
            this.pnlButtons.Size = new System.Drawing.Size(641, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(560, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(485, 7);
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(12, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(57, 13);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Название";
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(12, 25);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(617, 20);
            this.tbTitle.TabIndex = 0;
            // 
            // tbPublisher
            // 
            this.tbPublisher.BackColor = System.Drawing.Color.White;
            this.tbPublisher.Location = new System.Drawing.Point(262, 64);
            this.tbPublisher.Name = "tbPublisher";
            this.tbPublisher.ReadOnly = true;
            this.tbPublisher.Size = new System.Drawing.Size(334, 20);
            this.tbPublisher.TabIndex = 2;
            // 
            // lblPublisher
            // 
            this.lblPublisher.AutoSize = true;
            this.lblPublisher.Location = new System.Drawing.Point(262, 48);
            this.lblPublisher.Name = "lblPublisher";
            this.lblPublisher.Size = new System.Drawing.Size(56, 13);
            this.lblPublisher.TabIndex = 3;
            this.lblPublisher.Text = "Издатель";
            // 
            // tbISBN
            // 
            this.tbISBN.Location = new System.Drawing.Point(12, 64);
            this.tbISBN.Name = "tbISBN";
            this.tbISBN.Size = new System.Drawing.Size(244, 20);
            this.tbISBN.TabIndex = 1;
            // 
            // lblISBN
            // 
            this.lblISBN.AutoSize = true;
            this.lblISBN.Location = new System.Drawing.Point(12, 48);
            this.lblISBN.Name = "lblISBN";
            this.lblISBN.Size = new System.Drawing.Size(32, 13);
            this.lblISBN.TabIndex = 5;
            this.lblISBN.Text = "ISBN";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(328, 87);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(25, 13);
            this.lblYear.TabIndex = 7;
            this.lblYear.Text = "Год";
            // 
            // lblPages
            // 
            this.lblPages.AutoSize = true;
            this.lblPages.Location = new System.Drawing.Point(390, 87);
            this.lblPages.Name = "lblPages";
            this.lblPages.Size = new System.Drawing.Size(85, 13);
            this.lblPages.TabIndex = 9;
            this.lblPages.Text = "Кол-во страниц";
            // 
            // tbBBK
            // 
            this.tbBBK.Location = new System.Drawing.Point(12, 103);
            this.tbBBK.Name = "tbBBK";
            this.tbBBK.Size = new System.Drawing.Size(215, 20);
            this.tbBBK.TabIndex = 4;
            // 
            // lblBBK
            // 
            this.lblBBK.AutoSize = true;
            this.lblBBK.Location = new System.Drawing.Point(12, 87);
            this.lblBBK.Name = "lblBBK";
            this.lblBBK.Size = new System.Drawing.Size(28, 13);
            this.lblBBK.TabIndex = 11;
            this.lblBBK.Text = "ББК";
            // 
            // tbA_Znak
            // 
            this.tbA_Znak.Location = new System.Drawing.Point(233, 103);
            this.tbA_Znak.Name = "tbA_Znak";
            this.tbA_Znak.Size = new System.Drawing.Size(89, 20);
            this.tbA_Znak.TabIndex = 5;
            // 
            // lblA_Znak
            // 
            this.lblA_Znak.AutoSize = true;
            this.lblA_Znak.Location = new System.Drawing.Point(234, 87);
            this.lblA_Znak.Name = "lblA_Znak";
            this.lblA_Znak.Size = new System.Drawing.Size(88, 13);
            this.lblA_Znak.TabIndex = 13;
            this.lblA_Znak.Text = "Авторский знак";
            // 
            // lblVol
            // 
            this.lblVol.AutoSize = true;
            this.lblVol.Location = new System.Drawing.Point(481, 87);
            this.lblVol.Name = "lblVol";
            this.lblVol.Size = new System.Drawing.Size(28, 13);
            this.lblVol.TabIndex = 15;
            this.lblVol.Text = "Том";
            // 
            // lblVer
            // 
            this.lblVer.AutoSize = true;
            this.lblVer.Location = new System.Drawing.Point(543, 87);
            this.lblVer.Name = "lblVer";
            this.lblVer.Size = new System.Drawing.Size(86, 13);
            this.lblVer.TabIndex = 17;
            this.lblVer.Text = "Номер издания";
            // 
            // tbTyp
            // 
            this.tbTyp.BackColor = System.Drawing.Color.White;
            this.tbTyp.Location = new System.Drawing.Point(12, 142);
            this.tbTyp.Name = "tbTyp";
            this.tbTyp.ReadOnly = true;
            this.tbTyp.Size = new System.Drawing.Size(492, 20);
            this.tbTyp.TabIndex = 10;
            // 
            // lblTyp
            // 
            this.lblTyp.AutoSize = true;
            this.lblTyp.Location = new System.Drawing.Point(12, 126);
            this.lblTyp.Name = "lblTyp";
            this.lblTyp.Size = new System.Drawing.Size(71, 13);
            this.lblTyp.TabIndex = 19;
            this.lblTyp.Text = "Тип издания";
            // 
            // tbAnnotation
            // 
            this.tbAnnotation.Location = new System.Drawing.Point(12, 339);
            this.tbAnnotation.Multiline = true;
            this.tbAnnotation.Name = "tbAnnotation";
            this.tbAnnotation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbAnnotation.Size = new System.Drawing.Size(617, 116);
            this.tbAnnotation.TabIndex = 12;
            // 
            // lblAnnotation
            // 
            this.lblAnnotation.AutoSize = true;
            this.lblAnnotation.Location = new System.Drawing.Point(12, 323);
            this.lblAnnotation.Name = "lblAnnotation";
            this.lblAnnotation.Size = new System.Drawing.Size(61, 13);
            this.lblAnnotation.TabIndex = 21;
            this.lblAnnotation.Text = "Аннотация";
            // 
            // numYear
            // 
            this.numYear.Location = new System.Drawing.Point(328, 103);
            this.numYear.Maximum = new decimal(new int[] {
            2999,
            0,
            0,
            0});
            this.numYear.Name = "numYear";
            this.numYear.Size = new System.Drawing.Size(56, 20);
            this.numYear.TabIndex = 6;
            this.numYear.Value = new decimal(new int[] {
            2015,
            0,
            0,
            0});
            // 
            // numPages
            // 
            this.numPages.Location = new System.Drawing.Point(390, 103);
            this.numPages.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPages.Name = "numPages";
            this.numPages.Size = new System.Drawing.Size(85, 20);
            this.numPages.TabIndex = 7;
            // 
            // numVol
            // 
            this.numVol.Location = new System.Drawing.Point(481, 103);
            this.numVol.Name = "numVol";
            this.numVol.Size = new System.Drawing.Size(56, 20);
            this.numVol.TabIndex = 8;
            // 
            // numVer
            // 
            this.numVer.Location = new System.Drawing.Point(543, 103);
            this.numVer.Name = "numVer";
            this.numVer.Size = new System.Drawing.Size(86, 20);
            this.numVer.TabIndex = 9;
            // 
            // btnPublisher
            // 
            this.btnPublisher.Location = new System.Drawing.Point(602, 64);
            this.btnPublisher.Name = "btnPublisher";
            this.btnPublisher.Size = new System.Drawing.Size(27, 20);
            this.btnPublisher.TabIndex = 3;
            this.btnPublisher.Text = "...";
            this.btnPublisher.UseVisualStyleBackColor = true;
            this.btnPublisher.Click += new System.EventHandler(this.btnPublisher_Click);
            // 
            // btnTyp
            // 
            this.btnTyp.Location = new System.Drawing.Point(510, 142);
            this.btnTyp.Name = "btnTyp";
            this.btnTyp.Size = new System.Drawing.Size(27, 20);
            this.btnTyp.TabIndex = 11;
            this.btnTyp.Text = "...";
            this.btnTyp.UseVisualStyleBackColor = true;
            this.btnTyp.Click += new System.EventHandler(this.btnTyp_Click);
            // 
            // btnOut
            // 
            this.btnOut.Image = ((System.Drawing.Image)(resources.GetObject("btnOut.Image")));
            this.btnOut.Location = new System.Drawing.Point(543, 138);
            this.btnOut.Name = "btnOut";
            this.btnOut.Size = new System.Drawing.Size(86, 28);
            this.btnOut.TabIndex = 22;
            this.btnOut.Text = "Выдать ...";
            this.btnOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOut.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOut.UseVisualStyleBackColor = true;
            this.btnOut.Click += new System.EventHandler(this.btnOut_Click);
            // 
            // ucMemberEditor1
            // 
            this.ucMemberEditor1.Location = new System.Drawing.Point(12, 181);
            this.ucMemberEditor1.Name = "ucMemberEditor1";
            this.ucMemberEditor1.Size = new System.Drawing.Size(258, 139);
            this.ucMemberEditor1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Участники";
            // 
            // ucExemplarsEditor1
            // 
            this.ucExemplarsEditor1.Location = new System.Drawing.Point(276, 181);
            this.ucExemplarsEditor1.Name = "ucExemplarsEditor1";
            this.ucExemplarsEditor1.Size = new System.Drawing.Size(353, 139);
            this.ucExemplarsEditor1.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Экземпляры";
            // 
            // frmEditBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 504);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ucExemplarsEditor1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ucMemberEditor1);
            this.Controls.Add(this.btnOut);
            this.Controls.Add(this.btnTyp);
            this.Controls.Add(this.btnPublisher);
            this.Controls.Add(this.numVer);
            this.Controls.Add(this.numVol);
            this.Controls.Add(this.numPages);
            this.Controls.Add(this.numYear);
            this.Controls.Add(this.tbAnnotation);
            this.Controls.Add(this.lblAnnotation);
            this.Controls.Add(this.tbTyp);
            this.Controls.Add(this.lblTyp);
            this.Controls.Add(this.lblVer);
            this.Controls.Add(this.lblVol);
            this.Controls.Add(this.tbA_Znak);
            this.Controls.Add(this.lblA_Znak);
            this.Controls.Add(this.tbBBK);
            this.Controls.Add(this.lblBBK);
            this.Controls.Add(this.lblPages);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.tbISBN);
            this.Controls.Add(this.lblISBN);
            this.Controls.Add(this.tbPublisher);
            this.Controls.Add(this.lblPublisher);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditBook";
            this.Text = "Добавление книги";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.tbTitle, 0);
            this.Controls.SetChildIndex(this.lblPublisher, 0);
            this.Controls.SetChildIndex(this.tbPublisher, 0);
            this.Controls.SetChildIndex(this.lblISBN, 0);
            this.Controls.SetChildIndex(this.tbISBN, 0);
            this.Controls.SetChildIndex(this.lblYear, 0);
            this.Controls.SetChildIndex(this.lblPages, 0);
            this.Controls.SetChildIndex(this.lblBBK, 0);
            this.Controls.SetChildIndex(this.tbBBK, 0);
            this.Controls.SetChildIndex(this.lblA_Znak, 0);
            this.Controls.SetChildIndex(this.tbA_Znak, 0);
            this.Controls.SetChildIndex(this.lblVol, 0);
            this.Controls.SetChildIndex(this.lblVer, 0);
            this.Controls.SetChildIndex(this.lblTyp, 0);
            this.Controls.SetChildIndex(this.tbTyp, 0);
            this.Controls.SetChildIndex(this.lblAnnotation, 0);
            this.Controls.SetChildIndex(this.tbAnnotation, 0);
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.numYear, 0);
            this.Controls.SetChildIndex(this.numPages, 0);
            this.Controls.SetChildIndex(this.numVol, 0);
            this.Controls.SetChildIndex(this.numVer, 0);
            this.Controls.SetChildIndex(this.btnPublisher, 0);
            this.Controls.SetChildIndex(this.btnTyp, 0);
            this.Controls.SetChildIndex(this.btnOut, 0);
            this.Controls.SetChildIndex(this.ucMemberEditor1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.ucExemplarsEditor1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numVer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblPublisher;
        private System.Windows.Forms.Label lblISBN;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblPages;
        private System.Windows.Forms.Label lblBBK;
        private System.Windows.Forms.Label lblA_Znak;
        private System.Windows.Forms.Label lblVol;
        private System.Windows.Forms.Label lblVer;
        private System.Windows.Forms.Label lblTyp;
        private System.Windows.Forms.Label lblAnnotation;
        private System.Windows.Forms.Button btnPublisher;
        private System.Windows.Forms.Button btnTyp;
        public System.Windows.Forms.TextBox tbTitle;
        public System.Windows.Forms.TextBox tbPublisher;
        public System.Windows.Forms.TextBox tbISBN;
        public System.Windows.Forms.TextBox tbBBK;
        public System.Windows.Forms.TextBox tbA_Znak;
        public System.Windows.Forms.TextBox tbTyp;
        public System.Windows.Forms.TextBox tbAnnotation;
        public System.Windows.Forms.NumericUpDown numYear;
        public System.Windows.Forms.NumericUpDown numPages;
        public System.Windows.Forms.NumericUpDown numVol;
        public System.Windows.Forms.NumericUpDown numVer;
        public System.Windows.Forms.Button btnOut;
        private System.Windows.Forms.Label label1;
        public ucMemberEditor ucMemberEditor1;
        private System.Windows.Forms.Label label2;
        public ucExemplarsEditor ucExemplarsEditor1;
    }
}