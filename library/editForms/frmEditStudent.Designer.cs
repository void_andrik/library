﻿namespace library
{
    partial class frmEditStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblBirthDate = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.dpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.gbCard = new System.Windows.Forms.GroupBox();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.tbCardNumber = new System.Windows.Forms.TextBox();
            this.pnlButtons.SuspendLayout();
            this.gbStudent.SuspendLayout();
            this.gbCard.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 357);
            this.pnlButtons.Size = new System.Drawing.Size(438, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(357, 7);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnOk.Location = new System.Drawing.Point(282, 7);
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.lblPhone);
            this.gbStudent.Controls.Add(this.lblAddress);
            this.gbStudent.Controls.Add(this.lblGender);
            this.gbStudent.Controls.Add(this.lblBirthDate);
            this.gbStudent.Controls.Add(this.lblLastName);
            this.gbStudent.Controls.Add(this.lblMiddleName);
            this.gbStudent.Controls.Add(this.lblFirstName);
            this.gbStudent.Controls.Add(this.tbPhone);
            this.gbStudent.Controls.Add(this.tbAddress);
            this.gbStudent.Controls.Add(this.cbGender);
            this.gbStudent.Controls.Add(this.dpBirthDate);
            this.gbStudent.Controls.Add(this.tbLastName);
            this.gbStudent.Controls.Add(this.tbMiddleName);
            this.gbStudent.Controls.Add(this.tbFirstName);
            this.gbStudent.Location = new System.Drawing.Point(12, 84);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Size = new System.Drawing.Size(421, 268);
            this.gbStudent.TabIndex = 17;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Читатель";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(8, 219);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(52, 13);
            this.lblPhone.TabIndex = 30;
            this.lblPhone.Text = "Телефон";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(8, 131);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(38, 13);
            this.lblAddress.TabIndex = 29;
            this.lblAddress.Text = "Адрес";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(351, 219);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(27, 13);
            this.lblGender.TabIndex = 28;
            this.lblGender.Text = "Пол";
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.AutoSize = true;
            this.lblBirthDate.Location = new System.Drawing.Point(218, 219);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(86, 13);
            this.lblBirthDate.TabIndex = 27;
            this.lblBirthDate.Text = "Дата рождения";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(8, 92);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(54, 13);
            this.lblLastName.TabIndex = 26;
            this.lblLastName.Text = "Отчество";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(6, 53);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(29, 13);
            this.lblMiddleName.TabIndex = 25;
            this.lblMiddleName.Text = "Имя";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(6, 14);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(56, 13);
            this.lblFirstName.TabIndex = 24;
            this.lblFirstName.Text = "Фамилия";
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(6, 235);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(209, 20);
            this.tbPhone.TabIndex = 23;
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(6, 147);
            this.tbAddress.Multiline = true;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbAddress.Size = new System.Drawing.Size(406, 69);
            this.tbAddress.TabIndex = 22;
            // 
            // cbGender
            // 
            this.cbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Items.AddRange(new object[] {
            "М",
            "Ж"});
            this.cbGender.Location = new System.Drawing.Point(354, 235);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(58, 21);
            this.cbGender.TabIndex = 18;
            // 
            // dpBirthDate
            // 
            this.dpBirthDate.Location = new System.Drawing.Point(221, 235);
            this.dpBirthDate.Name = "dpBirthDate";
            this.dpBirthDate.Size = new System.Drawing.Size(127, 20);
            this.dpBirthDate.TabIndex = 17;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(6, 108);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(406, 20);
            this.tbLastName.TabIndex = 21;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Location = new System.Drawing.Point(6, 69);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.Size = new System.Drawing.Size(406, 20);
            this.tbMiddleName.TabIndex = 20;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(6, 30);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(406, 20);
            this.tbFirstName.TabIndex = 19;
            // 
            // gbCard
            // 
            this.gbCard.Controls.Add(this.lblEndDate);
            this.gbCard.Controls.Add(this.dpEndDate);
            this.gbCard.Controls.Add(this.lblCardNumber);
            this.gbCard.Controls.Add(this.tbCardNumber);
            this.gbCard.Location = new System.Drawing.Point(12, 12);
            this.gbCard.Name = "gbCard";
            this.gbCard.Size = new System.Drawing.Size(421, 66);
            this.gbCard.TabIndex = 18;
            this.gbCard.TabStop = false;
            this.gbCard.Text = "Читательский билет";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(282, 16);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(52, 13);
            this.lblEndDate.TabIndex = 29;
            this.lblEndDate.Text = "Годен до";
            // 
            // dpEndDate
            // 
            this.dpEndDate.Location = new System.Drawing.Point(285, 32);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(127, 20);
            this.dpEndDate.TabIndex = 28;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.Location = new System.Drawing.Point(6, 16);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(133, 13);
            this.lblCardNumber.TabIndex = 16;
            this.lblCardNumber.Text = "№ читательского билета";
            // 
            // tbCardNumber
            // 
            this.tbCardNumber.Location = new System.Drawing.Point(6, 32);
            this.tbCardNumber.Name = "tbCardNumber";
            this.tbCardNumber.Size = new System.Drawing.Size(273, 20);
            this.tbCardNumber.TabIndex = 15;
            // 
            // frmNewStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 400);
            this.Controls.Add(this.gbCard);
            this.Controls.Add(this.gbStudent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewStudent";
            this.Text = "Добавить читателя";
            this.Controls.SetChildIndex(this.gbStudent, 0);
            this.Controls.SetChildIndex(this.gbCard, 0);
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.pnlButtons.ResumeLayout(false);
            this.gbStudent.ResumeLayout(false);
            this.gbStudent.PerformLayout();
            this.gbCard.ResumeLayout(false);
            this.gbCard.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblBirthDate;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.Label lblFirstName;
        public System.Windows.Forms.TextBox tbPhone;
        public System.Windows.Forms.TextBox tbAddress;
        public System.Windows.Forms.ComboBox cbGender;
        public System.Windows.Forms.DateTimePicker dpBirthDate;
        public System.Windows.Forms.TextBox tbLastName;
        public System.Windows.Forms.TextBox tbMiddleName;
        public System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label lblEndDate;
        public System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label lblCardNumber;
        public System.Windows.Forms.TextBox tbCardNumber;
        public System.Windows.Forms.GroupBox gbCard;
    }
}