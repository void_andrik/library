﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmEditStudent : frmOkCancel
    {
        bool _checkCardNUm;

        public frmEditStudent(bool checkCardNum=false)
        {
            InitializeComponent();
            cbGender.SelectedIndex = 0;
            dpEndDate.Value = DateTime.Now.AddYears(1);
            this._checkCardNUm = checkCardNum;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            librDataSetTableAdapters.cardsTableAdapter adapter = new librDataSetTableAdapters.cardsTableAdapter();
            if (!string.IsNullOrEmpty(tbCardNumber.Text))
            {
                int cnt = (int)adapter.checkCardNum(tbCardNumber.Text);
                if (!_checkCardNUm || cnt == 0)
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                else
                    MessageBox.Show("Такой билет уже есть", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Номер читательского билета не может быть пустым", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
