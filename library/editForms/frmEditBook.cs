﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmEditBook : frmOkCancel
    {
        private int _book_id;
        public int exmplr_id = -1;
        private bool isout = false;

        public frmEditBook(int book_id = -1, bool isout=false)
        {
            InitializeComponent();
            tbPublisher.Tag = -1;
            tbTyp.Tag = -1;
            numYear.Value = DateTime.Now.Year;
            _book_id = book_id;
            ucMemberEditor1._book_id = book_id;
            ucExemplarsEditor1._book_id = book_id;
            this.isout = isout;
        }

        private void btnPublisher_Click(object sender, EventArgs e)
        {
            frmPublishers pub = new frmPublishers();
            if (pub.ShowDialog() == DialogResult.OK && pub.gridEditor.SelectedRows.Count==1)
            {
                int pub_id = (int)pub.gridEditor.SelectedRows[0].Cells[0].Value;
                string pub_name = pub.gridEditor.SelectedRows[0].Cells[1].Value.ToString();
                string pub_city = pub.gridEditor.SelectedRows[0].Cells[2].Value.ToString();
                string pub_text;
                if (!string.IsNullOrEmpty(pub_city))
                    pub_text = string.Format("{0}: {1}", pub_name, pub_city);
                else
                    pub_text = pub_name;
                tbPublisher.Tag = pub_id;
                tbPublisher.Text = pub_text;
            }
        }

        private void btnTyp_Click(object sender, EventArgs e)
        {
            frmBookTypes bt = new frmBookTypes();
            if (bt.ShowDialog() == DialogResult.OK && bt.gridEditor.SelectedRows.Count==1)
            {
                int typ_id = (int)bt.gridEditor.SelectedRows[0].Cells[0].Value;
                string typ_name = bt.gridEditor.SelectedRows[0].Cells[1].Value.ToString();
                tbTyp.Tag = typ_id;
                tbTyp.Text = typ_name;
            }

        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            exmplr_id = ucExemplarsEditor1.exmplr_id;
            if (exmplr_id != -1)
            {
                if (isout)
                    btnOk.PerformClick();
                else
                {
                    api.bookOut(null, null, new List<int> { exmplr_id });
                    btnCancel.PerformClick();
                }
            }
       }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int authorCnt = 0;
            foreach( memberItem mi in ucMemberEditor1.items)
                if (mi._mtyp_mtyp_id==1)
                    authorCnt++;
            if (authorCnt == 0)
            {
                MessageBox.Show("Нельзя добавить книгу без автора", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.None;
            }
        }
    }
}
