﻿namespace library
{
    partial class frmMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin5 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin5 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient13 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient29 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin5 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient5 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient30 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient14 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient31 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient5 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient32 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient33 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient15 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient34 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient35 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeLibrarianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.booksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.newBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.delBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.findBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repUsedBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repOutOfDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dictionariesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.publishersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bookTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.memberTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cardStatusesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.librariansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMain = new System.Windows.Forms.ToolStrip();
            this.dockPnl = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.menuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.studentsToolStripMenuItem,
            this.booksToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.dictionariesToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuMain.Size = new System.Drawing.Size(904, 24);
            this.menuMain.TabIndex = 1;
            this.menuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeLibrarianToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // changeLibrarianToolStripMenuItem
            // 
            this.changeLibrarianToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("changeLibrarianToolStripMenuItem.Image")));
            this.changeLibrarianToolStripMenuItem.Name = "changeLibrarianToolStripMenuItem";
            this.changeLibrarianToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.changeLibrarianToolStripMenuItem.Text = "Сменить пользователя ...";
            this.changeLibrarianToolStripMenuItem.Click += new System.EventHandler(this.changeLibrarianToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStudentToolStripMenuItem,
            this.cardServiceToolStripMenuItem,
            this.findStudentToolStripMenuItem});
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.studentsToolStripMenuItem.Text = "Читатели";
            // 
            // newStudentToolStripMenuItem
            // 
            this.newStudentToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newStudentToolStripMenuItem.Image")));
            this.newStudentToolStripMenuItem.Name = "newStudentToolStripMenuItem";
            this.newStudentToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.newStudentToolStripMenuItem.Text = "Добавить читателя ...";
            this.newStudentToolStripMenuItem.Click += new System.EventHandler(this.newStudentToolStripMenuItem_Click);
            // 
            // cardServiceToolStripMenuItem
            // 
            this.cardServiceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cardServiceToolStripMenuItem.Image")));
            this.cardServiceToolStripMenuItem.Name = "cardServiceToolStripMenuItem";
            this.cardServiceToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.cardServiceToolStripMenuItem.Text = "Замена читательского билета ...";
            this.cardServiceToolStripMenuItem.Click += new System.EventHandler(this.cardServiceToolStripMenuItem_Click);
            // 
            // findStudentToolStripMenuItem
            // 
            this.findStudentToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("findStudentToolStripMenuItem.Image")));
            this.findStudentToolStripMenuItem.Name = "findStudentToolStripMenuItem";
            this.findStudentToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.findStudentToolStripMenuItem.Text = "Поиск читателя ...";
            this.findStudentToolStripMenuItem.Click += new System.EventHandler(this.findStudentToolStripMenuItem_Click);
            // 
            // booksToolStripMenuItem
            // 
            this.booksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bookInToolStripMenuItem,
            this.bookOutToolStripMenuItem,
            this.toolStripMenuItem1,
            this.newBookToolStripMenuItem,
            this.delBookToolStripMenuItem,
            this.toolStripMenuItem4,
            this.findBookToolStripMenuItem});
            this.booksToolStripMenuItem.Name = "booksToolStripMenuItem";
            this.booksToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.booksToolStripMenuItem.Text = "Книги";
            // 
            // bookInToolStripMenuItem
            // 
            this.bookInToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("bookInToolStripMenuItem.Image")));
            this.bookInToolStripMenuItem.Name = "bookInToolStripMenuItem";
            this.bookInToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.bookInToolStripMenuItem.Text = "Возврат книг ...";
            this.bookInToolStripMenuItem.Click += new System.EventHandler(this.bookInToolStripMenuItem_Click);
            // 
            // bookOutToolStripMenuItem
            // 
            this.bookOutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("bookOutToolStripMenuItem.Image")));
            this.bookOutToolStripMenuItem.Name = "bookOutToolStripMenuItem";
            this.bookOutToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.bookOutToolStripMenuItem.Text = "Выдача книг ...";
            this.bookOutToolStripMenuItem.Click += new System.EventHandler(this.bookOutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 6);
            // 
            // newBookToolStripMenuItem
            // 
            this.newBookToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newBookToolStripMenuItem.Image")));
            this.newBookToolStripMenuItem.Name = "newBookToolStripMenuItem";
            this.newBookToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.newBookToolStripMenuItem.Text = "Добавить книгу ...";
            this.newBookToolStripMenuItem.Click += new System.EventHandler(this.newBookToolStripMenuItem_Click);
            // 
            // delBookToolStripMenuItem
            // 
            this.delBookToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("delBookToolStripMenuItem.Image")));
            this.delBookToolStripMenuItem.Name = "delBookToolStripMenuItem";
            this.delBookToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.delBookToolStripMenuItem.Text = "Удалить книгу";
            this.delBookToolStripMenuItem.Click += new System.EventHandler(this.delBookToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(168, 6);
            // 
            // findBookToolStripMenuItem
            // 
            this.findBookToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("findBookToolStripMenuItem.Image")));
            this.findBookToolStripMenuItem.Name = "findBookToolStripMenuItem";
            this.findBookToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.findBookToolStripMenuItem.Text = "Поиск книги ...";
            this.findBookToolStripMenuItem.Click += new System.EventHandler(this.findBookToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.repUsedBooksToolStripMenuItem,
            this.repOutOfDateToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Отчёты";
            // 
            // repUsedBooksToolStripMenuItem
            // 
            this.repUsedBooksToolStripMenuItem.Name = "repUsedBooksToolStripMenuItem";
            this.repUsedBooksToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.repUsedBooksToolStripMenuItem.Text = "Книги на руках";
            this.repUsedBooksToolStripMenuItem.Click += new System.EventHandler(this.repUsedBooksToolStripMenuItem_Click);
            // 
            // repOutOfDateToolStripMenuItem
            // 
            this.repOutOfDateToolStripMenuItem.Name = "repOutOfDateToolStripMenuItem";
            this.repOutOfDateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.repOutOfDateToolStripMenuItem.Text = "Просроченные книги";
            this.repOutOfDateToolStripMenuItem.Click += new System.EventHandler(this.repOutOfDateToolStripMenuItem_Click);
            // 
            // dictionariesToolStripMenuItem
            // 
            this.dictionariesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.publishersToolStripMenuItem,
            this.bookTypesToolStripMenuItem,
            this.toolStripMenuItem5,
            this.memberTypesToolStripMenuItem,
            this.cardStatusesToolStripMenuItem,
            this.librariansToolStripMenuItem});
            this.dictionariesToolStripMenuItem.Name = "dictionariesToolStripMenuItem";
            this.dictionariesToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.dictionariesToolStripMenuItem.Text = "Справочники";
            // 
            // publishersToolStripMenuItem
            // 
            this.publishersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("publishersToolStripMenuItem.Image")));
            this.publishersToolStripMenuItem.Name = "publishersToolStripMenuItem";
            this.publishersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.publishersToolStripMenuItem.Text = "Издательства";
            this.publishersToolStripMenuItem.Click += new System.EventHandler(this.publishersToolStripMenuItem_Click);
            // 
            // bookTypesToolStripMenuItem
            // 
            this.bookTypesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("bookTypesToolStripMenuItem.Image")));
            this.bookTypesToolStripMenuItem.Name = "bookTypesToolStripMenuItem";
            this.bookTypesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.bookTypesToolStripMenuItem.Text = "Типы изданий";
            this.bookTypesToolStripMenuItem.Click += new System.EventHandler(this.bookTypesToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(160, 6);
            // 
            // memberTypesToolStripMenuItem
            // 
            this.memberTypesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("memberTypesToolStripMenuItem.Image")));
            this.memberTypesToolStripMenuItem.Name = "memberTypesToolStripMenuItem";
            this.memberTypesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.memberTypesToolStripMenuItem.Text = "Типы участников";
            this.memberTypesToolStripMenuItem.Click += new System.EventHandler(this.memberTypesToolStripMenuItem_Click);
            // 
            // cardStatusesToolStripMenuItem
            // 
            this.cardStatusesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cardStatusesToolStripMenuItem.Image")));
            this.cardStatusesToolStripMenuItem.Name = "cardStatusesToolStripMenuItem";
            this.cardStatusesToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cardStatusesToolStripMenuItem.Text = "Статусы билетов";
            this.cardStatusesToolStripMenuItem.Click += new System.EventHandler(this.cardStatusesToolStripMenuItem_Click);
            // 
            // librariansToolStripMenuItem
            // 
            this.librariansToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("librariansToolStripMenuItem.Image")));
            this.librariansToolStripMenuItem.Name = "librariansToolStripMenuItem";
            this.librariansToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.librariansToolStripMenuItem.Text = "Библиотекари";
            this.librariansToolStripMenuItem.Click += new System.EventHandler(this.librariansToolStripMenuItem_Click);
            // 
            // toolMain
            // 
            this.toolMain.Location = new System.Drawing.Point(0, 24);
            this.toolMain.Name = "toolMain";
            this.toolMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolMain.Size = new System.Drawing.Size(904, 25);
            this.toolMain.TabIndex = 2;
            this.toolMain.Text = "toolStrip1";
            // 
            // dockPnl
            // 
            this.dockPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPnl.DockBackColor = System.Drawing.Color.DimGray;
            this.dockPnl.Location = new System.Drawing.Point(0, 49);
            this.dockPnl.Name = "dockPnl";
            this.dockPnl.Size = new System.Drawing.Size(904, 536);
            dockPanelGradient13.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient13.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin5.DockStripGradient = dockPanelGradient13;
            tabGradient29.EndColor = System.Drawing.SystemColors.Control;
            tabGradient29.StartColor = System.Drawing.SystemColors.Control;
            tabGradient29.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin5.TabGradient = tabGradient29;
            autoHideStripSkin5.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            dockPanelSkin5.AutoHideStripSkin = autoHideStripSkin5;
            tabGradient30.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient30.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient30.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient5.ActiveTabGradient = tabGradient30;
            dockPanelGradient14.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient14.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient5.DockStripGradient = dockPanelGradient14;
            tabGradient31.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient31.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient31.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient5.InactiveTabGradient = tabGradient31;
            dockPaneStripSkin5.DocumentGradient = dockPaneStripGradient5;
            dockPaneStripSkin5.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            tabGradient32.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient32.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient32.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient32.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient5.ActiveCaptionGradient = tabGradient32;
            tabGradient33.EndColor = System.Drawing.SystemColors.Control;
            tabGradient33.StartColor = System.Drawing.SystemColors.Control;
            tabGradient33.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient5.ActiveTabGradient = tabGradient33;
            dockPanelGradient15.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient15.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient5.DockStripGradient = dockPanelGradient15;
            tabGradient34.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient34.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient34.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient34.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient5.InactiveCaptionGradient = tabGradient34;
            tabGradient35.EndColor = System.Drawing.Color.Transparent;
            tabGradient35.StartColor = System.Drawing.Color.Transparent;
            tabGradient35.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient5.InactiveTabGradient = tabGradient35;
            dockPaneStripSkin5.ToolWindowGradient = dockPaneStripToolWindowGradient5;
            dockPanelSkin5.DockPaneStripSkin = dockPaneStripSkin5;
            this.dockPnl.Skin = dockPanelSkin5;
            this.dockPnl.TabIndex = 4;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(904, 585);
            this.Controls.Add(this.dockPnl);
            this.Controls.Add(this.toolMain);
            this.Controls.Add(this.menuMain);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuMain;
            this.Name = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStrip toolMain;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPnl;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dictionariesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memberTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem publishersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cardStatusesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cardServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bookOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem delBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repUsedBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repOutOfDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem findBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeLibrarianToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem librariansToolStripMenuItem;
    }
}

