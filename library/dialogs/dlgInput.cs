﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class dlgInput : Form
    {
        public dlgInput()
        {
            InitializeComponent();
        }

        public dlgInput(string caption, string def, string value)
        {
            InitializeComponent();
            this.Text = caption;
            lblDef.Text = def;
            tbValue.Text = value;
        }

        private void dlgInput_Load(object sender, EventArgs e)
        {

        }

        private void tbValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOk.PerformClick();
        }
    }
}
