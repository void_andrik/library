﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmPublishers : frmTableEditor
    {
        librDataSet.publishersDataTable table = new librDataSet.publishersDataTable();
        librDataSetTableAdapters.publishersTableAdapter adapter = new librDataSetTableAdapters.publishersTableAdapter();

        public frmPublishers()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            filterText = "Convert(pub_id, 'System.String') like '%{0}%' OR pub_name LIKE '%{0}%' OR pub_city LIKE '%{0}%'";
        }

        public void refresh_table()
        {
            rememberRow();
            adapter.Fill(table);
            bs.DataSource = table;
            restoreRow();
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[0].Visible = false;
            gridEditor.Columns[1].HeaderText = "Название";
            gridEditor.Columns[2].HeaderText = "Город";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "pub_id", "ID" }, { "pub_name", "TXT" }, { "pub_city", "TXT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "pub_id", "ID" }, { "pub_name", "Наименование" }, { "pub_city", "Город" } };
            frmRowEditor re = new frmRowEditor(i, d);
            if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string new_pub_name = re.getValue("pub_name");
                string new_pub_city = re.getValue("pub_city");
                adapter.Insert(new_pub_name, new_pub_city);
                refresh_table();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                int id = (int)gridEditor.SelectedRows[0].Cells["pub_id"].Value;
                Dictionary<string, string> i = new Dictionary<string, string> { { "pub_id", "ID" }, { "pub_name", "TXT" }, { "pub_city", "TXT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "pub_id", "ID" }, { "pub_name", "Наименование" }, { "pub_city", "Город" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("pub_id", id.ToString());
                v.Add("pub_name", gridEditor.SelectedRows[0].Cells["pub_name"].Value.ToString());
                v.Add("pub_city", gridEditor.SelectedRows[0].Cells["pub_city"].Value.ToString());
                frmRowEditor re = new frmRowEditor(i, d, v);
                if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string new_pub_name = re.getValue("pub_name");
                    string new_pub_city = re.getValue("pub_city");
                    adapter.Update(new_pub_name, new_pub_city, id);
                    refresh_table();
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["pub_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        private void frmPublishers_Load(object sender, EventArgs e)
        {
            refresh_table();
        }

    }
}
