﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmLibrarians : frmTableEditor
    {
        librDataSet.peoples_membersDataTable table = new librDataSet.peoples_membersDataTable();
        librDataSetTableAdapters.peoples_membersTableAdapter adapter = new librDataSetTableAdapters.peoples_membersTableAdapter();

        public frmLibrarians()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            btnDel.Enabled = false;
            filterText = "first_name+' '+middle_name+' '+last_name like '%{0}%'";
        }

        public void refresh_table()
        {
            rememberRow();
            adapter.FillLibrarians(table);
            bs.DataSource = table;
            restoreRow();
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[0].Visible = false;
            gridEditor.Columns[1].HeaderText = "Фамилия";
            gridEditor.Columns[2].HeaderText = "Имя";
            gridEditor.Columns[3].HeaderText = "Отчество";
            gridEditor.Columns[4].Visible = false;
            gridEditor.Columns[5].Visible = false;
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "ppl_id", "ID" }, { "first_name", "TXT" }, { "middle_name", "TXT" }, { "last_name", "TXT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "ppl_id", "ID" }, { "first_name", "Фамилия" }, { "middle_name", "Имя" }, { "last_name", "Отчество" } };
            frmRowEditor re = new frmRowEditor(i, d);
            if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string first_name = re.getValue("first_name");
                string middle_name = re.getValue("middle_name");
                string last_name = re.getValue("last_name");
                adapter.Insert(first_name, middle_name, last_name, 3);
                refresh_table();
            }

        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                int id = (int)gridEditor.SelectedRows[0].Cells["ppl_id"].Value;
                Dictionary<string, string> i = new Dictionary<string, string> { { "ppl_id", "ID" }, { "first_name", "TXT" }, { "middle_name", "TXT" }, { "last_name", "TXT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "ppl_id", "ID" }, { "first_name", "Фамилия" }, { "middle_name", "Имя" }, { "last_name", "Отчество" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("ppl_id", id.ToString());
                v.Add("first_name", gridEditor.SelectedRows[0].Cells["first_name"].Value.ToString());
                v.Add("middle_name", gridEditor.SelectedRows[0].Cells["middle_name"].Value.ToString());
                v.Add("last_name", gridEditor.SelectedRows[0].Cells["last_name"].Value.ToString());
                frmRowEditor re = new frmRowEditor(i, d, v);
                if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string first_name = re.getValue("first_name");
                    string middle_name = re.getValue("middle_name");
                    string last_name = re.getValue("last_name");
                    adapter.Update(first_name, middle_name, last_name, 3, id);
                    refresh_table();
                }
            }

        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["ppl_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        private void frmLibrarians_Load(object sender, EventArgs e)
        {
            refresh_table();
        }
    }
}
