﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmCardStatuses : frmTableEditor
    {
        librDataSet.card_statusesDataTable table = new librDataSet.card_statusesDataTable();
        librDataSetTableAdapters.card_statusesTableAdapter adapter = new librDataSetTableAdapters.card_statusesTableAdapter();

        public frmCardStatuses()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            filterText = "Convert(crs_id, 'System.String') like '%{0}%' OR def LIKE '%{0}%'";
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "crs_id", "ID" }, { "def", "TXT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "crs_id", "ID" }, { "def", "Наименование" } };
            frmRowEditor re = new frmRowEditor(i, d);
            if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string new_def = re.getValue("def");
                adapter.Insert(new_def);
                refresh_table();
            }
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                int id = (int)gridEditor.SelectedRows[0].Cells["crs_id"].Value;
                Dictionary<string, string> i = new Dictionary<string, string> { { "crs_id", "ID" }, { "def", "TXT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "crs_id", "ID" }, { "def", "Наименование" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("crs_id", id.ToString());
                v.Add("def", gridEditor.SelectedRows[0].Cells["def"].Value.ToString());
                frmRowEditor re = new frmRowEditor(i, d, v);
                if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string new_def = re.getValue("def");
                    adapter.Update(new_def, id);
                    refresh_table();
                }

            }
        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["crs_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        public void refresh_table()
        {
            rememberRow();
            adapter.Fill(table);
            bs.DataSource = table;
            restoreRow();
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[0].Visible = false;
            gridEditor.Columns[1].HeaderText = "Название";
        }

        private void frmCardStatuses_Load(object sender, EventArgs e)
        {
            refresh_table();
        }

    }
}
