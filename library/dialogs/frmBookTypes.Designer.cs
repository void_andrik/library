﻿namespace library
{
    partial class frmBookTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            this.pnlButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbFilter
            // 
            this.tbFilter.Size = new System.Drawing.Size(434, 20);
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 331);
            this.pnlButtons.Size = new System.Drawing.Size(434, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(353, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(278, 7);
            // 
            // frmBookTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 374);
            this.Name = "frmBookTypes";
            this.Text = "Типы изданий";
            this.Load += new System.EventHandler(this.frmBookTypes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            this.pnlButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}