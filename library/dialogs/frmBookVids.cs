﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmBookVids : frmTableEditor
    {
        librDataSet.book_vidsDataTable table = new librDataSet.book_vidsDataTable();
        librDataSetTableAdapters.book_vidsTableAdapter adapter = new librDataSetTableAdapters.book_vidsTableAdapter();

        public frmBookVids()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            filterText = "Convert(vid_id, 'System.String') like '%{0}%' OR vid_name LIKE '%{0}%' OR def LIKE '%{0}%'";
        }

        public void refresh_table()
        {
            rememberRow();
            adapter.Fill(table);
            bs.DataSource = table;
            restoreRow();
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[0].Visible = false;
            gridEditor.Columns[1].HeaderText = "Название";
            gridEditor.Columns[2].HeaderText = "Описание";
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "vid_id", "ID" }, { "vid_name", "TXT" }, { "def", "TXT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "vid_id", "ID" }, { "vid_name", "Название" }, { "def", "Описание" } };
            frmRowEditor re = new frmRowEditor(i, d);
            if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string new_name = re.getValue("vid_name");
                string new_def = re.getValue("def");
                adapter.Insert(new_name, new_def);
                refresh_table();
            }
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                int id = (int)gridEditor.SelectedRows[0].Cells["vid_id"].Value;
                Dictionary<string, string> i = new Dictionary<string, string> { { "vid_id", "ID" }, { "vid_name", "TXT" }, { "def", "TXT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "vid_id", "ID" }, { "vid_name", "Название" }, { "def", "Описание" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("vid_id", id.ToString());
                v.Add("vid_name", gridEditor.SelectedRows[0].Cells["vid_name"].Value.ToString());
                v.Add("def", gridEditor.SelectedRows[0].Cells["def"].Value.ToString());
                frmRowEditor re = new frmRowEditor(i, d, v);
                if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string new_name = re.getValue("vid_name");
                    string new_def = re.getValue("def");
                    adapter.Update(new_name, new_def, id);
                    refresh_table();
                }
            }
        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["vid_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        private void rmBookVids_Load(object sender, EventArgs e)
        {
            refresh_table();
        }
    }
}
