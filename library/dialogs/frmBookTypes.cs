﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmBookTypes : frmTableEditor
    {
        librDataSet.book_typesDataTable table = new librDataSet.book_typesDataTable();
        librDataSetTableAdapters.book_typesTableAdapter adapter = new librDataSetTableAdapters.book_typesTableAdapter();

        public frmBookTypes()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            filterText = "Convert(typ_id, 'System.String') like '%{0}%' OR typ_name LIKE '%{0}%' OR def LIKE '%{0}%'";
        }

        public void refresh_table()
        {
            rememberRow();
            adapter.Fill(table);
            bs.DataSource = table;
            restoreRow();
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[0].Visible = false;
            gridEditor.Columns[1].HeaderText = "Название";
            gridEditor.Columns[2].HeaderText = "Описание";
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> i = new Dictionary<string, string> { { "typ_id", "ID" }, { "typ_name", "TXT" }, { "def", "TXT" } };
            Dictionary<string, string> d = new Dictionary<string, string> { { "typ_id", "ID" }, { "typ_name", "Название" }, { "def", "Описание" } };
            frmRowEditor re = new frmRowEditor(i, d);
            if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string new_name = re.getValue("typ_name");
                string new_def = re.getValue("def");
                adapter.Insert(new_name, new_def);
                refresh_table();
            }
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                int id = (int)gridEditor.SelectedRows[0].Cells["typ_id"].Value;
                Dictionary<string, string> i = new Dictionary<string, string> { { "typ_id", "ID" }, { "typ_name", "TXT" }, { "def", "TXT" } };
                Dictionary<string, string> d = new Dictionary<string, string> { { "typ_id", "ID" }, { "typ_name", "Название" }, { "def", "Описание" } };
                Dictionary<string, string> v = new Dictionary<string, string>();
                v.Add("typ_id", id.ToString());
                v.Add("typ_name", gridEditor.SelectedRows[0].Cells["typ_name"].Value.ToString());
                v.Add("def", gridEditor.SelectedRows[0].Cells["def"].Value.ToString());
                frmRowEditor re = new frmRowEditor(i, d, v);
                if (re.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string new_name = re.getValue("typ_name");
                    string new_def = re.getValue("def");
                    adapter.Update(new_name, new_def, id);
                    refresh_table();
                }
            }
        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["typ_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        private void frmBookTypes_Load(object sender, EventArgs e)
        {
            refresh_table();
        }
    }
}
