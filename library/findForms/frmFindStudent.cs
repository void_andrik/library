﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmFindStudent : frmOkCancel
    {
        public int crd_id = -1;
        public int ppl_id = -1;
        public string cardNum = "";

        public frmFindStudent()
        {
            InitializeComponent();
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                doSearch();
        }

        private void doSearch()
        {
            //if (!string.IsNullOrEmpty(tbSearch.Text))
                if (rbFIO.Checked)
                {
                    librDataSetTableAdapters.findCrdByFIOTableAdapter adapter = new librDataSetTableAdapters.findCrdByFIOTableAdapter();
                    librDataSet.findCrdByFIODataTable table = new librDataSet.findCrdByFIODataTable();
                    adapter.Fill(table, tbSearch.Text);
                    gvSearchResult.DataSource = table;
                }
                else
                {
                    librDataSetTableAdapters.findCrdByCardTableAdapter adapter = new librDataSetTableAdapters.findCrdByCardTableAdapter();
                    librDataSet.findCrdByCardDataTable table = new librDataSet.findCrdByCardDataTable();
                    adapter.Fill(table, tbSearch.Text);
                    gvSearchResult.DataSource = table;
                }
        }

        private void gvSearchResult_SelectionChanged(object sender, EventArgs e)
        {
            if (gvSearchResult.SelectedRows.Count > 0)
            {
                crd_id = (int)gvSearchResult.SelectedRows[0].Cells["crd"].Value;
                ppl_id = (int)gvSearchResult.SelectedRows[0].Cells["ppl"].Value;
                cardNum = gvSearchResult.SelectedRows[0].Cells["card_num"].Value.ToString();
            }
            else
            {
                crd_id = -1;
                ppl_id = -1;
                cardNum = "";
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            doSearch();
        }

        private void gvSearchResult_DoubleClick(object sender, EventArgs e)
        {
            btnOk.PerformClick();
        }
    }
}
