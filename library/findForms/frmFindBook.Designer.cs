﻿namespace library
{
    partial class frmFindBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbSearchResult = new System.Windows.Forms.GroupBox();
            this.gvSearchResult = new System.Windows.Forms.DataGridView();
            this.bookid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cntexDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.findBookBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.librDataSet = new library.librDataSet();
            this.gbFindBy = new System.Windows.Forms.GroupBox();
            this.rbInvNumber = new System.Windows.Forms.RadioButton();
            this.rbAuthor = new System.Windows.Forms.RadioButton();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.rbTitle = new System.Windows.Forms.RadioButton();
            this.findBookTableAdapter = new library.librDataSetTableAdapters.findBookTableAdapter();
            this.pnlButtons.SuspendLayout();
            this.gbSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.findBookBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).BeginInit();
            this.gbFindBy.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 363);
            this.pnlButtons.Size = new System.Drawing.Size(577, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(496, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(421, 7);
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // gbSearchResult
            // 
            this.gbSearchResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSearchResult.Controls.Add(this.gvSearchResult);
            this.gbSearchResult.Location = new System.Drawing.Point(12, 90);
            this.gbSearchResult.Name = "gbSearchResult";
            this.gbSearchResult.Size = new System.Drawing.Size(559, 267);
            this.gbSearchResult.TabIndex = 4;
            this.gbSearchResult.TabStop = false;
            this.gbSearchResult.Text = "Результаты поиска";
            // 
            // gvSearchResult
            // 
            this.gvSearchResult.AllowUserToAddRows = false;
            this.gvSearchResult.AutoGenerateColumns = false;
            this.gvSearchResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvSearchResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvSearchResult.ColumnHeadersHeight = 25;
            this.gvSearchResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bookid,
            this.titleDataGridViewTextBoxColumn,
            this.cntexDataGridViewTextBoxColumn});
            this.gvSearchResult.DataSource = this.findBookBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvSearchResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvSearchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvSearchResult.Location = new System.Drawing.Point(3, 16);
            this.gvSearchResult.MultiSelect = false;
            this.gvSearchResult.Name = "gvSearchResult";
            this.gvSearchResult.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvSearchResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvSearchResult.RowHeadersVisible = false;
            this.gvSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvSearchResult.Size = new System.Drawing.Size(553, 248);
            this.gvSearchResult.TabIndex = 0;
            this.gvSearchResult.DoubleClick += new System.EventHandler(this.gvSearchResult_DoubleClick);
            // 
            // bookid
            // 
            this.bookid.DataPropertyName = "book_id";
            this.bookid.HeaderText = "book_id";
            this.bookid.Name = "bookid";
            this.bookid.ReadOnly = true;
            this.bookid.Visible = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Название";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cntexDataGridViewTextBoxColumn
            // 
            this.cntexDataGridViewTextBoxColumn.DataPropertyName = "cnt_ex";
            this.cntexDataGridViewTextBoxColumn.FillWeight = 30F;
            this.cntexDataGridViewTextBoxColumn.HeaderText = "Кол-во экз.";
            this.cntexDataGridViewTextBoxColumn.Name = "cntexDataGridViewTextBoxColumn";
            this.cntexDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // findBookBindingSource
            // 
            this.findBookBindingSource.DataMember = "findBook";
            this.findBookBindingSource.DataSource = this.librDataSet;
            this.findBookBindingSource.CurrentChanged += new System.EventHandler(this.findBookBindingSource_CurrentChanged);
            // 
            // librDataSet
            // 
            this.librDataSet.DataSetName = "librDataSet";
            this.librDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gbFindBy
            // 
            this.gbFindBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFindBy.Controls.Add(this.rbInvNumber);
            this.gbFindBy.Controls.Add(this.rbAuthor);
            this.gbFindBy.Controls.Add(this.btnSearch);
            this.gbFindBy.Controls.Add(this.tbSearch);
            this.gbFindBy.Controls.Add(this.rbTitle);
            this.gbFindBy.Location = new System.Drawing.Point(12, 12);
            this.gbFindBy.Name = "gbFindBy";
            this.gbFindBy.Size = new System.Drawing.Size(559, 72);
            this.gbFindBy.TabIndex = 3;
            this.gbFindBy.TabStop = false;
            this.gbFindBy.Text = "Поиск по";
            // 
            // rbInvNumber
            // 
            this.rbInvNumber.AutoSize = true;
            this.rbInvNumber.Location = new System.Drawing.Point(155, 19);
            this.rbInvNumber.Name = "rbInvNumber";
            this.rbInvNumber.Size = new System.Drawing.Size(88, 17);
            this.rbInvNumber.TabIndex = 5;
            this.rbInvNumber.Text = "Инв. номеру";
            this.rbInvNumber.UseVisualStyleBackColor = true;
            // 
            // rbAuthor
            // 
            this.rbAuthor.AutoSize = true;
            this.rbAuthor.Location = new System.Drawing.Point(89, 19);
            this.rbAuthor.Name = "rbAuthor";
            this.rbAuthor.Size = new System.Drawing.Size(60, 17);
            this.rbAuthor.TabIndex = 4;
            this.rbAuthor.Text = "Автору";
            this.rbAuthor.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(478, 40);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(6, 42);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(466, 20);
            this.tbSearch.TabIndex = 2;
            this.tbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyUp);
            // 
            // rbTitle
            // 
            this.rbTitle.AutoSize = true;
            this.rbTitle.Checked = true;
            this.rbTitle.Location = new System.Drawing.Point(6, 19);
            this.rbTitle.Name = "rbTitle";
            this.rbTitle.Size = new System.Drawing.Size(77, 17);
            this.rbTitle.TabIndex = 0;
            this.rbTitle.TabStop = true;
            this.rbTitle.Text = "Названию";
            this.rbTitle.UseVisualStyleBackColor = true;
            // 
            // findBookTableAdapter
            // 
            this.findBookTableAdapter.ClearBeforeFill = true;
            // 
            // frmFindBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 406);
            this.Controls.Add(this.gbSearchResult);
            this.Controls.Add(this.gbFindBy);
            this.Name = "frmFindBook";
            this.Text = "Поиск книги";
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.gbFindBy, 0);
            this.Controls.SetChildIndex(this.gbSearchResult, 0);
            this.pnlButtons.ResumeLayout(false);
            this.gbSearchResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.findBookBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).EndInit();
            this.gbFindBy.ResumeLayout(false);
            this.gbFindBy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSearchResult;
        private System.Windows.Forms.DataGridView gvSearchResult;
        private System.Windows.Forms.GroupBox gbFindBy;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.RadioButton rbTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookyearDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource findBookBindingSource;
        private librDataSet librDataSet;
        private librDataSetTableAdapters.findBookTableAdapter findBookTableAdapter;
        private System.Windows.Forms.RadioButton rbInvNumber;
        private System.Windows.Forms.RadioButton rbAuthor;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookid;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cntexDataGridViewTextBoxColumn;
    }
}