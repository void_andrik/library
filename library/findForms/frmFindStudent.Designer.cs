﻿namespace library
{
    partial class frmFindStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbFindBy = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.rbCard = new System.Windows.Forms.RadioButton();
            this.rbFIO = new System.Windows.Forms.RadioButton();
            this.gbSearchResult = new System.Windows.Forms.GroupBox();
            this.gvSearchResult = new System.Windows.Forms.DataGridView();
            this.crd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ppl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.card_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.middlenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.findCrdByFIOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.librDataSet = new library.librDataSet();
            this.findCrdByFIOTableAdapter = new library.librDataSetTableAdapters.findCrdByFIOTableAdapter();
            this.pnlButtons.SuspendLayout();
            this.gbFindBy.SuspendLayout();
            this.gbSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.findCrdByFIOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 360);
            this.pnlButtons.Size = new System.Drawing.Size(594, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(513, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(438, 7);
            // 
            // gbFindBy
            // 
            this.gbFindBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFindBy.Controls.Add(this.btnSearch);
            this.gbFindBy.Controls.Add(this.tbSearch);
            this.gbFindBy.Controls.Add(this.rbCard);
            this.gbFindBy.Controls.Add(this.rbFIO);
            this.gbFindBy.Location = new System.Drawing.Point(3, 12);
            this.gbFindBy.Name = "gbFindBy";
            this.gbFindBy.Size = new System.Drawing.Size(585, 72);
            this.gbFindBy.TabIndex = 1;
            this.gbFindBy.TabStop = false;
            this.gbFindBy.Text = "Поиск по";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(504, 40);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(6, 42);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(492, 20);
            this.tbSearch.TabIndex = 2;
            this.tbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyUp);
            // 
            // rbCard
            // 
            this.rbCard.AutoSize = true;
            this.rbCard.Location = new System.Drawing.Point(90, 19);
            this.rbCard.Name = "rbCard";
            this.rbCard.Size = new System.Drawing.Size(151, 17);
            this.rbCard.TabIndex = 1;
            this.rbCard.Text = "№ читательского билета";
            this.rbCard.UseVisualStyleBackColor = true;
            // 
            // rbFIO
            // 
            this.rbFIO.AutoSize = true;
            this.rbFIO.Checked = true;
            this.rbFIO.Location = new System.Drawing.Point(6, 19);
            this.rbFIO.Name = "rbFIO";
            this.rbFIO.Size = new System.Drawing.Size(61, 17);
            this.rbFIO.TabIndex = 0;
            this.rbFIO.TabStop = true;
            this.rbFIO.Text = "Ф.И.О.";
            this.rbFIO.UseVisualStyleBackColor = true;
            // 
            // gbSearchResult
            // 
            this.gbSearchResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSearchResult.Controls.Add(this.gvSearchResult);
            this.gbSearchResult.Location = new System.Drawing.Point(3, 90);
            this.gbSearchResult.Name = "gbSearchResult";
            this.gbSearchResult.Size = new System.Drawing.Size(585, 271);
            this.gbSearchResult.TabIndex = 2;
            this.gbSearchResult.TabStop = false;
            this.gbSearchResult.Text = "Результаты поиска";
            // 
            // gvSearchResult
            // 
            this.gvSearchResult.AllowUserToAddRows = false;
            this.gvSearchResult.AutoGenerateColumns = false;
            this.gvSearchResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvSearchResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvSearchResult.ColumnHeadersHeight = 25;
            this.gvSearchResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.crd,
            this.ppl,
            this.card_num,
            this.firstnameDataGridViewTextBoxColumn,
            this.middlenameDataGridViewTextBoxColumn,
            this.lastnameDataGridViewTextBoxColumn});
            this.gvSearchResult.DataSource = this.findCrdByFIOBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvSearchResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvSearchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvSearchResult.Location = new System.Drawing.Point(3, 16);
            this.gvSearchResult.MultiSelect = false;
            this.gvSearchResult.Name = "gvSearchResult";
            this.gvSearchResult.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvSearchResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvSearchResult.RowHeadersVisible = false;
            this.gvSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvSearchResult.Size = new System.Drawing.Size(579, 252);
            this.gvSearchResult.TabIndex = 0;
            this.gvSearchResult.SelectionChanged += new System.EventHandler(this.gvSearchResult_SelectionChanged);
            this.gvSearchResult.DoubleClick += new System.EventHandler(this.gvSearchResult_DoubleClick);
            // 
            // crd
            // 
            this.crd.DataPropertyName = "crd_id";
            this.crd.HeaderText = "crd_id";
            this.crd.Name = "crd";
            this.crd.ReadOnly = true;
            this.crd.Visible = false;
            // 
            // ppl
            // 
            this.ppl.DataPropertyName = "ppl_id";
            this.ppl.HeaderText = "ppl_id";
            this.ppl.Name = "ppl";
            this.ppl.ReadOnly = true;
            this.ppl.Visible = false;
            // 
            // card_num
            // 
            this.card_num.DataPropertyName = "card_num";
            this.card_num.HeaderText = "№ билета";
            this.card_num.Name = "card_num";
            this.card_num.ReadOnly = true;
            // 
            // firstnameDataGridViewTextBoxColumn
            // 
            this.firstnameDataGridViewTextBoxColumn.DataPropertyName = "first_name";
            this.firstnameDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.firstnameDataGridViewTextBoxColumn.Name = "firstnameDataGridViewTextBoxColumn";
            this.firstnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // middlenameDataGridViewTextBoxColumn
            // 
            this.middlenameDataGridViewTextBoxColumn.DataPropertyName = "middle_name";
            this.middlenameDataGridViewTextBoxColumn.HeaderText = "Имя";
            this.middlenameDataGridViewTextBoxColumn.Name = "middlenameDataGridViewTextBoxColumn";
            this.middlenameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastnameDataGridViewTextBoxColumn
            // 
            this.lastnameDataGridViewTextBoxColumn.DataPropertyName = "last_name";
            this.lastnameDataGridViewTextBoxColumn.HeaderText = "Отчество";
            this.lastnameDataGridViewTextBoxColumn.Name = "lastnameDataGridViewTextBoxColumn";
            this.lastnameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // findCrdByFIOBindingSource
            // 
            this.findCrdByFIOBindingSource.DataMember = "findCrdByFIO";
            this.findCrdByFIOBindingSource.DataSource = this.librDataSet;
            // 
            // librDataSet
            // 
            this.librDataSet.DataSetName = "librDataSet";
            this.librDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // findCrdByFIOTableAdapter
            // 
            this.findCrdByFIOTableAdapter.ClearBeforeFill = true;
            // 
            // frmFindStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 403);
            this.Controls.Add(this.gbSearchResult);
            this.Controls.Add(this.gbFindBy);
            this.Name = "frmFindStudent";
            this.Text = "Поиск читателя";
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.gbFindBy, 0);
            this.Controls.SetChildIndex(this.gbSearchResult, 0);
            this.pnlButtons.ResumeLayout(false);
            this.gbFindBy.ResumeLayout(false);
            this.gbFindBy.PerformLayout();
            this.gbSearchResult.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvSearchResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.findCrdByFIOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbFindBy;
        private System.Windows.Forms.RadioButton rbCard;
        private System.Windows.Forms.RadioButton rbFIO;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.GroupBox gbSearchResult;
        private System.Windows.Forms.DataGridView gvSearchResult;
        private System.Windows.Forms.BindingSource findCrdByFIOBindingSource;
        private librDataSet librDataSet;
        private librDataSetTableAdapters.findCrdByFIOTableAdapter findCrdByFIOTableAdapter;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn crd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ppl;
        private System.Windows.Forms.DataGridViewTextBoxColumn card_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn middlenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastnameDataGridViewTextBoxColumn;

    }
}