﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmFindBook : frmOkCancel
    {
        public int book_id = -1;

        public frmFindBook()
        {
            InitializeComponent();
            gvSearchResult.AutoGenerateColumns = false;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            doSearch();
        }

        private void doSearch()
        {
            //if (!string.IsNullOrEmpty(tbSearch.Text))
            //{
            if (rbTitle.Checked)
            {
                librDataSetTableAdapters.findBookTableAdapter adapter = new librDataSetTableAdapters.findBookTableAdapter();
                librDataSet.findBookDataTable table = new librDataSet.findBookDataTable();
                adapter.Fill(table, tbSearch.Text);
                gvSearchResult.DataSource = table;
            }
            else if (rbInvNumber.Checked)
            {
                librDataSetTableAdapters.findBookTableAdapter adapter = new librDataSetTableAdapters.findBookTableAdapter();
                librDataSet.findBookDataTable table = new librDataSet.findBookDataTable();
                int invNumber = -1;
                int.TryParse(tbSearch.Text, out invNumber);
                adapter.FillByInvNumber(table, invNumber);
                gvSearchResult.DataSource = table;
            }
            else if (rbAuthor.Checked)
            {
                librDataSetTableAdapters.findBookTableAdapter adapter = new librDataSetTableAdapters.findBookTableAdapter();
                librDataSet.findBookDataTable table = new librDataSet.findBookDataTable();
                adapter.FillByAuthor(table, tbSearch.Text);
                gvSearchResult.DataSource = table;
            }
            //}
        }

        private void gvSearchResult_DoubleClick(object sender, EventArgs e)
        {
            btnOk.PerformClick();
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                doSearch();
        }

        private void findBookBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            book_id = (int)gvSearchResult.SelectedRows[0].Cells["bookid"].Value;
        }
    }
}
