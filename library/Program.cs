﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace library
{
    static class Program
    {
        public static WeifenLuo.WinFormsUI.Docking.DockPanel _dp;
        public static int Librarian = 0;
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
        }
    }
}
