﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class edtMemberTypes : frmTableEditor
    {
        librDataSet.member_typesDataTable table = new librDataSet.member_typesDataTable();
        librDataSetTableAdapters.member_typesTableAdapter adapter = new librDataSetTableAdapters.member_typesTableAdapter();

        public edtMemberTypes()
        {
            InitializeComponent();
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
        }

        public void refresh_table()
        {
            adapter.Fill(table);
            gridEditor.DataSource = table;
            gridEditor.Columns[0].HeaderText = "ID";
            gridEditor.Columns[1].HeaderText = "Название";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            dlgInput di = new dlgInput("Добавление типа участника","Введите название","");
            if (di.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                adapter.Insert(di.tbValue.Text);
                refresh_table();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
            {
                dlgInput di = new dlgInput("Изменение типа участника", "Введите название", gridEditor.SelectedRows[0].Cells["def"].Value.ToString());
                if (di.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["mtyp_id"].Value;
                    adapter.Update(di.tbValue.Text, id);
                    refresh_table();
                }
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (gridEditor.SelectedRows.Count > 0)
                if (MessageBox.Show("Удалить запись?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = (int)gridEditor.SelectedRows[0].Cells["mtyp_id"].Value;
                    adapter.Delete(id);
                    refresh_table();
                }
        }

        private void edtMemberTypes_Load(object sender, EventArgs e)
        {
            refresh_table();
        }
    }
}
