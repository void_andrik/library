﻿namespace library
{
    partial class frmBookOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBookOut));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.tbCardNumber = new System.Windows.Forms.TextBox();
            this.tsStudent = new System.Windows.Forms.ToolStrip();
            this.btnStudent = new System.Windows.Forms.ToolStripButton();
            this.gbBookList = new System.Windows.Forms.GroupBox();
            this.gvBooksOut = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsBooks = new System.Windows.Forms.ToolStrip();
            this.btnAddBook = new System.Windows.Forms.ToolStripButton();
            this.btnDelBook = new System.Windows.Forms.ToolStripButton();
            this.gbOutParams = new System.Windows.Forms.GroupBox();
            this.numDays = new System.Windows.Forms.NumericUpDown();
            this.lblDays = new System.Windows.Forms.Label();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gbStudent.SuspendLayout();
            this.tsStudent.SuspendLayout();
            this.gbBookList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksOut)).BeginInit();
            this.tsBooks.SuspendLayout();
            this.gbOutParams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDays)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 451);
            this.pnlButtons.Size = new System.Drawing.Size(778, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(697, 7);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(622, 7);
            this.btnOk.Text = "Выдать";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gbOutParams);
            this.splitContainer1.Panel1.Controls.Add(this.gbStudent);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbBookList);
            this.splitContainer1.Size = new System.Drawing.Size(778, 451);
            this.splitContainer1.SplitterDistance = 259;
            this.splitContainer1.TabIndex = 2;
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.lblLastName);
            this.gbStudent.Controls.Add(this.lblMiddleName);
            this.gbStudent.Controls.Add(this.lblFirstName);
            this.gbStudent.Controls.Add(this.tbLastName);
            this.gbStudent.Controls.Add(this.tbMiddleName);
            this.gbStudent.Controls.Add(this.tbFirstName);
            this.gbStudent.Controls.Add(this.lblCardNumber);
            this.gbStudent.Controls.Add(this.tbCardNumber);
            this.gbStudent.Controls.Add(this.tsStudent);
            this.gbStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStudent.Location = new System.Drawing.Point(0, 0);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Size = new System.Drawing.Size(259, 206);
            this.gbStudent.TabIndex = 2;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Читатель";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(8, 160);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(54, 13);
            this.lblLastName.TabIndex = 32;
            this.lblLastName.Text = "Отчество";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(6, 121);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(29, 13);
            this.lblMiddleName.TabIndex = 31;
            this.lblMiddleName.Text = "Имя";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(6, 82);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(56, 13);
            this.lblFirstName.TabIndex = 30;
            this.lblFirstName.Text = "Фамилия";
            // 
            // tbLastName
            // 
            this.tbLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLastName.Location = new System.Drawing.Point(6, 176);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.ReadOnly = true;
            this.tbLastName.Size = new System.Drawing.Size(245, 20);
            this.tbLastName.TabIndex = 29;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMiddleName.Location = new System.Drawing.Point(6, 137);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.ReadOnly = true;
            this.tbMiddleName.Size = new System.Drawing.Size(245, 20);
            this.tbMiddleName.TabIndex = 28;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFirstName.Location = new System.Drawing.Point(6, 98);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.ReadOnly = true;
            this.tbFirstName.Size = new System.Drawing.Size(245, 20);
            this.tbFirstName.TabIndex = 27;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.Location = new System.Drawing.Point(6, 46);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(133, 13);
            this.lblCardNumber.TabIndex = 23;
            this.lblCardNumber.Text = "№ читательского билета";
            // 
            // tbCardNumber
            // 
            this.tbCardNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCardNumber.Location = new System.Drawing.Point(6, 62);
            this.tbCardNumber.Name = "tbCardNumber";
            this.tbCardNumber.ReadOnly = true;
            this.tbCardNumber.Size = new System.Drawing.Size(245, 20);
            this.tbCardNumber.TabIndex = 22;
            // 
            // tsStudent
            // 
            this.tsStudent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStudent});
            this.tsStudent.Location = new System.Drawing.Point(3, 16);
            this.tsStudent.Name = "tsStudent";
            this.tsStudent.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsStudent.Size = new System.Drawing.Size(253, 25);
            this.tsStudent.TabIndex = 0;
            this.tsStudent.Text = "toolStrip2";
            // 
            // btnStudent
            // 
            this.btnStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnStudent.Image")));
            this.btnStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStudent.Name = "btnStudent";
            this.btnStudent.Size = new System.Drawing.Size(122, 22);
            this.btnStudent.Text = "Выбрать читателя";
            this.btnStudent.Click += new System.EventHandler(this.btnStudent_Click);
            // 
            // gbBookList
            // 
            this.gbBookList.Controls.Add(this.gvBooksOut);
            this.gbBookList.Controls.Add(this.tsBooks);
            this.gbBookList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbBookList.Location = new System.Drawing.Point(0, 0);
            this.gbBookList.Name = "gbBookList";
            this.gbBookList.Size = new System.Drawing.Size(515, 451);
            this.gbBookList.TabIndex = 0;
            this.gbBookList.TabStop = false;
            this.gbBookList.Text = "Список книг";
            // 
            // gvBooksOut
            // 
            this.gvBooksOut.AllowUserToAddRows = false;
            this.gvBooksOut.AllowUserToDeleteRows = false;
            this.gvBooksOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksOut.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.gvBooksOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvBooksOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colTitle});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvBooksOut.DefaultCellStyle = dataGridViewCellStyle11;
            this.gvBooksOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvBooksOut.Location = new System.Drawing.Point(3, 41);
            this.gvBooksOut.Name = "gvBooksOut";
            this.gvBooksOut.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksOut.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.gvBooksOut.RowHeadersVisible = false;
            this.gvBooksOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvBooksOut.Size = new System.Drawing.Size(509, 407);
            this.gvBooksOut.TabIndex = 1;
            // 
            // colID
            // 
            this.colID.DataPropertyName = "exmplr_id";
            this.colID.FillWeight = 30F;
            this.colID.HeaderText = "Инв. номер";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            // 
            // colTitle
            // 
            this.colTitle.DataPropertyName = "title";
            this.colTitle.HeaderText = "Название";
            this.colTitle.Name = "colTitle";
            this.colTitle.ReadOnly = true;
            // 
            // tsBooks
            // 
            this.tsBooks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddBook,
            this.btnDelBook});
            this.tsBooks.Location = new System.Drawing.Point(3, 16);
            this.tsBooks.Name = "tsBooks";
            this.tsBooks.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsBooks.Size = new System.Drawing.Size(509, 25);
            this.tsBooks.TabIndex = 0;
            this.tsBooks.Text = "toolStrip1";
            // 
            // btnAddBook
            // 
            this.btnAddBook.Image = ((System.Drawing.Image)(resources.GetObject("btnAddBook.Image")));
            this.btnAddBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(128, 22);
            this.btnAddBook.Text = "Добавить к выдаче";
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // btnDelBook
            // 
            this.btnDelBook.Image = ((System.Drawing.Image)(resources.GetObject("btnDelBook.Image")));
            this.btnDelBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelBook.Name = "btnDelBook";
            this.btnDelBook.Size = new System.Drawing.Size(127, 22);
            this.btnDelBook.Text = "Удалить из выдачи";
            this.btnDelBook.Click += new System.EventHandler(this.btnDelBook_Click);
            // 
            // gbOutParams
            // 
            this.gbOutParams.Controls.Add(this.lblDays);
            this.gbOutParams.Controls.Add(this.numDays);
            this.gbOutParams.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbOutParams.Location = new System.Drawing.Point(0, 206);
            this.gbOutParams.Name = "gbOutParams";
            this.gbOutParams.Size = new System.Drawing.Size(259, 64);
            this.gbOutParams.TabIndex = 3;
            this.gbOutParams.TabStop = false;
            this.gbOutParams.Text = "Выдача";
            // 
            // numDays
            // 
            this.numDays.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.numDays.Location = new System.Drawing.Point(6, 32);
            this.numDays.Name = "numDays";
            this.numDays.Size = new System.Drawing.Size(245, 20);
            this.numDays.TabIndex = 0;
            this.numDays.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(6, 16);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(99, 13);
            this.lblDays.TabIndex = 33;
            this.lblDays.Text = "Выдать на ... дней";
            // 
            // frmBookOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 494);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmBookOut";
            this.Text = "Выдача книг";
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.pnlButtons.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gbStudent.ResumeLayout(false);
            this.gbStudent.PerformLayout();
            this.tsStudent.ResumeLayout(false);
            this.tsStudent.PerformLayout();
            this.gbBookList.ResumeLayout(false);
            this.gbBookList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksOut)).EndInit();
            this.tsBooks.ResumeLayout(false);
            this.tsBooks.PerformLayout();
            this.gbOutParams.ResumeLayout(false);
            this.gbOutParams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.GroupBox gbBookList;
        private System.Windows.Forms.ToolStrip tsBooks;
        private System.Windows.Forms.ToolStripButton btnAddBook;
        private System.Windows.Forms.ToolStripButton btnDelBook;
        private System.Windows.Forms.DataGridView gvBooksOut;
        private System.Windows.Forms.ToolStrip tsStudent;
        private System.Windows.Forms.ToolStripButton btnStudent;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.Label lblFirstName;
        public System.Windows.Forms.TextBox tbLastName;
        public System.Windows.Forms.TextBox tbMiddleName;
        public System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label lblCardNumber;
        public System.Windows.Forms.TextBox tbCardNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTitle;
        private System.Windows.Forms.GroupBox gbOutParams;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.NumericUpDown numDays;
    }
}