﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmChangeCard : frmOkCancel
    {
        public frmChangeCard()
        {
            InitializeComponent();
            dpEndDate.Value = DateTime.Now.AddYears(1);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            librDataSetTableAdapters.cardsTableAdapter adapter = new librDataSetTableAdapters.cardsTableAdapter();
            if (!string.IsNullOrEmpty(tbCardNumber.Text) & tbCardNumber.Text != tbOldCardNumber.Text)
            {
                int cnt = (int)adapter.checkCardNum(tbCardNumber.Text);
                if (cnt==0)
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                else
                    MessageBox.Show("Такой билет уже есть", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("Номер читательского билета не может быть пустым или совпадать с уже имеющимся", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
