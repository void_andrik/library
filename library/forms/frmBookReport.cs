﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace library
{
    public partial class frmBookReport : frmOkCancel
    {
        public enum reportType { allBooks, expiredBooks };

        private reportType _repTyp;
        public frmBookReport(reportType repTyp)
        {
            InitializeComponent();
            pnlButtons.Visible = false;
            this._repTyp = repTyp;
        }

        private void frmBookReport_Load(object sender, EventArgs e)
        {
            if (this._repTyp == reportType.allBooks)
            {
                librDataSetTableAdapters.rep1TableAdapter a = new librDataSetTableAdapters.rep1TableAdapter();
                librDataSet.rep1DataTable t = new librDataSet.rep1DataTable();
                a.Fill(t);
                DataTable dt = t;
                dt.TableName = "rep1DS";
                reportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportDataSource source = new ReportDataSource("rep1DS", dt);
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(source);
                reportViewer1.LocalReport.ReportPath = "reports\\repOnHands.rdlc";
                reportViewer1.LocalReport.Refresh();
                reportViewer1.RefreshReport();
            }
            else if (this._repTyp == reportType.expiredBooks)
            {
                librDataSetTableAdapters.rep1TableAdapter a = new librDataSetTableAdapters.rep1TableAdapter();
                librDataSet.rep1DataTable t = new librDataSet.rep1DataTable();
                a.FillByOutOfDate(t, DateTime.Now.ToShortDateString());
                DataTable dt = t;
                dt.TableName = "rep1DS";
                reportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportDataSource source = new ReportDataSource("rep1DS", dt);
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.ReportPath = "reports\\repOnHands.rdlc";
                reportViewer1.LocalReport.DataSources.Add(source);
                reportViewer1.LocalReport.Refresh();
                reportViewer1.RefreshReport();
            }
            this.reportViewer1.RefreshReport();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            frmBookReport_Load(null,null);
        }

    }
}
