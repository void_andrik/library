﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmLogon : frmOkCancel
    {
        public frmLogon()
        {
            InitializeComponent();
            librDataSetTableAdapters.peoples_membersTableAdapter adapter = new librDataSetTableAdapters.peoples_membersTableAdapter();
            librDataSet.peoples_membersDataTable table = new librDataSet.peoples_membersDataTable();
            adapter.FillLibrarians(table);
            cbLibrarian.DataSource = table;
            cbLibrarian.DisplayMember = "full_fio";
            cbLibrarian.ValueMember = "ppl_id";
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Program.Librarian = (int)cbLibrarian.SelectedValue;
        }
    }
}
