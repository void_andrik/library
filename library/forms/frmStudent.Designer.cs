﻿namespace library
{
    partial class frmStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStudent));
            this.gbBooks = new System.Windows.Forms.GroupBox();
            this.btnRefreshTable = new System.Windows.Forms.Button();
            this.tcBooks = new System.Windows.Forms.TabControl();
            this.pgOnHands = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pgOutOfDate = new System.Windows.Forms.TabPage();
            this.pgHistory = new System.Windows.Forms.TabPage();
            this.gbCard = new System.Windows.Forms.GroupBox();
            this.btnViewHistory = new System.Windows.Forms.Button();
            this.btnChangeCard = new System.Windows.Forms.Button();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.tbCardNumber = new System.Windows.Forms.TextBox();
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.btnChangeStudent = new System.Windows.Forms.Button();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblBirthDate = new System.Windows.Forms.Label();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.tbPhone = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.dpBirthDate = new System.Windows.Forms.DateTimePicker();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.toolTipMain = new System.Windows.Forms.ToolTip(this.components);
            this.gbBookActions = new System.Windows.Forms.GroupBox();
            this.backBook = new System.Windows.Forms.Button();
            this.getBook = new System.Windows.Forms.Button();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbBooks.SuspendLayout();
            this.tcBooks.SuspendLayout();
            this.pgOnHands.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pgOutOfDate.SuspendLayout();
            this.pgHistory.SuspendLayout();
            this.gbCard.SuspendLayout();
            this.gbStudent.SuspendLayout();
            this.gbBookActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // gbBooks
            // 
            this.gbBooks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbBooks.Controls.Add(this.btnRefreshTable);
            this.gbBooks.Controls.Add(this.tcBooks);
            this.gbBooks.Location = new System.Drawing.Point(366, 12);
            this.gbBooks.Name = "gbBooks";
            this.gbBooks.Size = new System.Drawing.Size(635, 475);
            this.gbBooks.TabIndex = 1;
            this.gbBooks.TabStop = false;
            this.gbBooks.Text = "Список книг";
            // 
            // btnRefreshTable
            // 
            this.btnRefreshTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshTable.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshTable.Image")));
            this.btnRefreshTable.ImageAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btnRefreshTable.Location = new System.Drawing.Point(602, 0);
            this.btnRefreshTable.Name = "btnRefreshTable";
            this.btnRefreshTable.Size = new System.Drawing.Size(23, 23);
            this.btnRefreshTable.TabIndex = 33;
            this.btnRefreshTable.TabStop = false;
            this.toolTipMain.SetToolTip(this.btnRefreshTable, "Обновить данные");
            this.btnRefreshTable.UseVisualStyleBackColor = true;
            this.btnRefreshTable.Click += new System.EventHandler(this.btnRefreshTable_Click);
            // 
            // tcBooks
            // 
            this.tcBooks.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tcBooks.Controls.Add(this.pgOnHands);
            this.tcBooks.Controls.Add(this.pgOutOfDate);
            this.tcBooks.Controls.Add(this.pgHistory);
            this.tcBooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcBooks.Location = new System.Drawing.Point(3, 16);
            this.tcBooks.Name = "tcBooks";
            this.tcBooks.SelectedIndex = 0;
            this.tcBooks.Size = new System.Drawing.Size(629, 456);
            this.tcBooks.TabIndex = 0;
            this.tcBooks.SelectedIndexChanged += new System.EventHandler(this.tcBooks_SelectedIndexChanged);
            // 
            // pgOnHands
            // 
            this.pgOnHands.Controls.Add(this.dataGridView1);
            this.pgOnHands.Location = new System.Drawing.Point(4, 25);
            this.pgOnHands.Name = "pgOnHands";
            this.pgOnHands.Padding = new System.Windows.Forms.Padding(3);
            this.pgOnHands.Size = new System.Drawing.Size(621, 427);
            this.pgOnHands.TabIndex = 0;
            this.pgOnHands.Text = "На руках";
            this.pgOnHands.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(615, 421);
            this.dataGridView1.TabIndex = 0;
            // 
            // pgOutOfDate
            // 
            this.pgOutOfDate.Controls.Add(this.dataGridView2);
            this.pgOutOfDate.Location = new System.Drawing.Point(4, 25);
            this.pgOutOfDate.Name = "pgOutOfDate";
            this.pgOutOfDate.Padding = new System.Windows.Forms.Padding(3);
            this.pgOutOfDate.Size = new System.Drawing.Size(621, 427);
            this.pgOutOfDate.TabIndex = 1;
            this.pgOutOfDate.Text = "Просроченные";
            this.pgOutOfDate.UseVisualStyleBackColor = true;
            // 
            // pgHistory
            // 
            this.pgHistory.Controls.Add(this.dataGridView3);
            this.pgHistory.Controls.Add(this.toolStrip1);
            this.pgHistory.Location = new System.Drawing.Point(4, 25);
            this.pgHistory.Name = "pgHistory";
            this.pgHistory.Size = new System.Drawing.Size(621, 427);
            this.pgHistory.TabIndex = 2;
            this.pgHistory.Text = "История";
            this.pgHistory.UseVisualStyleBackColor = true;
            // 
            // gbCard
            // 
            this.gbCard.Controls.Add(this.btnViewHistory);
            this.gbCard.Controls.Add(this.btnChangeCard);
            this.gbCard.Controls.Add(this.lblEndDate);
            this.gbCard.Controls.Add(this.dpEndDate);
            this.gbCard.Controls.Add(this.lblCardNumber);
            this.gbCard.Controls.Add(this.tbCardNumber);
            this.gbCard.Location = new System.Drawing.Point(12, 12);
            this.gbCard.Name = "gbCard";
            this.gbCard.Size = new System.Drawing.Size(348, 66);
            this.gbCard.TabIndex = 20;
            this.gbCard.TabStop = false;
            this.gbCard.Text = "Читательский билет";
            // 
            // btnViewHistory
            // 
            this.btnViewHistory.Enabled = false;
            this.btnViewHistory.Image = ((System.Drawing.Image)(resources.GetObject("btnViewHistory.Image")));
            this.btnViewHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnViewHistory.Location = new System.Drawing.Point(316, 0);
            this.btnViewHistory.Name = "btnViewHistory";
            this.btnViewHistory.Size = new System.Drawing.Size(23, 23);
            this.btnViewHistory.TabIndex = 31;
            this.btnViewHistory.TabStop = false;
            this.toolTipMain.SetToolTip(this.btnViewHistory, "История билетов");
            this.btnViewHistory.UseVisualStyleBackColor = true;
            // 
            // btnChangeCard
            // 
            this.btnChangeCard.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeCard.Image")));
            this.btnChangeCard.Location = new System.Drawing.Point(287, 0);
            this.btnChangeCard.Name = "btnChangeCard";
            this.btnChangeCard.Size = new System.Drawing.Size(23, 23);
            this.btnChangeCard.TabIndex = 30;
            this.btnChangeCard.TabStop = false;
            this.toolTipMain.SetToolTip(this.btnChangeCard, "Заменить читательский билет");
            this.btnChangeCard.UseVisualStyleBackColor = true;
            this.btnChangeCard.Click += new System.EventHandler(this.btnChangeCard_Click);
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(209, 16);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(52, 13);
            this.lblEndDate.TabIndex = 29;
            this.lblEndDate.Text = "Годен до";
            // 
            // dpEndDate
            // 
            this.dpEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpEndDate.Enabled = false;
            this.dpEndDate.Location = new System.Drawing.Point(212, 32);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(127, 20);
            this.dpEndDate.TabIndex = 28;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.Location = new System.Drawing.Point(6, 16);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(133, 13);
            this.lblCardNumber.TabIndex = 16;
            this.lblCardNumber.Text = "№ читательского билета";
            // 
            // tbCardNumber
            // 
            this.tbCardNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCardNumber.Location = new System.Drawing.Point(6, 32);
            this.tbCardNumber.Name = "tbCardNumber";
            this.tbCardNumber.ReadOnly = true;
            this.tbCardNumber.Size = new System.Drawing.Size(200, 20);
            this.tbCardNumber.TabIndex = 15;
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.btnChangeStudent);
            this.gbStudent.Controls.Add(this.lblPhone);
            this.gbStudent.Controls.Add(this.lblAddress);
            this.gbStudent.Controls.Add(this.lblGender);
            this.gbStudent.Controls.Add(this.lblBirthDate);
            this.gbStudent.Controls.Add(this.lblLastName);
            this.gbStudent.Controls.Add(this.lblMiddleName);
            this.gbStudent.Controls.Add(this.lblFirstName);
            this.gbStudent.Controls.Add(this.tbPhone);
            this.gbStudent.Controls.Add(this.tbAddress);
            this.gbStudent.Controls.Add(this.cbGender);
            this.gbStudent.Controls.Add(this.dpBirthDate);
            this.gbStudent.Controls.Add(this.tbLastName);
            this.gbStudent.Controls.Add(this.tbMiddleName);
            this.gbStudent.Controls.Add(this.tbFirstName);
            this.gbStudent.Location = new System.Drawing.Point(12, 84);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Size = new System.Drawing.Size(348, 268);
            this.gbStudent.TabIndex = 19;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Читатель";
            // 
            // btnChangeStudent
            // 
            this.btnChangeStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnChangeStudent.Image")));
            this.btnChangeStudent.Location = new System.Drawing.Point(316, 0);
            this.btnChangeStudent.Name = "btnChangeStudent";
            this.btnChangeStudent.Size = new System.Drawing.Size(23, 23);
            this.btnChangeStudent.TabIndex = 31;
            this.btnChangeStudent.TabStop = false;
            this.toolTipMain.SetToolTip(this.btnChangeStudent, "Изменить данные");
            this.btnChangeStudent.UseVisualStyleBackColor = true;
            this.btnChangeStudent.Click += new System.EventHandler(this.btnChangeStudent_Click);
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(8, 219);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(52, 13);
            this.lblPhone.TabIndex = 30;
            this.lblPhone.Text = "Телефон";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(8, 131);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(38, 13);
            this.lblAddress.TabIndex = 29;
            this.lblAddress.Text = "Адрес";
            // 
            // lblGender
            // 
            this.lblGender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(278, 219);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(27, 13);
            this.lblGender.TabIndex = 28;
            this.lblGender.Text = "Пол";
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBirthDate.AutoSize = true;
            this.lblBirthDate.Location = new System.Drawing.Point(145, 219);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(86, 13);
            this.lblBirthDate.TabIndex = 27;
            this.lblBirthDate.Text = "Дата рождения";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(8, 92);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(54, 13);
            this.lblLastName.TabIndex = 26;
            this.lblLastName.Text = "Отчество";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(6, 53);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(29, 13);
            this.lblMiddleName.TabIndex = 25;
            this.lblMiddleName.Text = "Имя";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(6, 14);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(56, 13);
            this.lblFirstName.TabIndex = 24;
            this.lblFirstName.Text = "Фамилия";
            // 
            // tbPhone
            // 
            this.tbPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPhone.Location = new System.Drawing.Point(6, 235);
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.ReadOnly = true;
            this.tbPhone.Size = new System.Drawing.Size(136, 20);
            this.tbPhone.TabIndex = 23;
            // 
            // tbAddress
            // 
            this.tbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddress.Location = new System.Drawing.Point(6, 147);
            this.tbAddress.Multiline = true;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.ReadOnly = true;
            this.tbAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbAddress.Size = new System.Drawing.Size(333, 69);
            this.tbAddress.TabIndex = 22;
            // 
            // cbGender
            // 
            this.cbGender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGender.Enabled = false;
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Items.AddRange(new object[] {
            "М",
            "Ж"});
            this.cbGender.Location = new System.Drawing.Point(281, 235);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(58, 21);
            this.cbGender.TabIndex = 18;
            // 
            // dpBirthDate
            // 
            this.dpBirthDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpBirthDate.Enabled = false;
            this.dpBirthDate.Location = new System.Drawing.Point(148, 235);
            this.dpBirthDate.Name = "dpBirthDate";
            this.dpBirthDate.Size = new System.Drawing.Size(127, 20);
            this.dpBirthDate.TabIndex = 17;
            // 
            // tbLastName
            // 
            this.tbLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLastName.Location = new System.Drawing.Point(6, 108);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.ReadOnly = true;
            this.tbLastName.Size = new System.Drawing.Size(333, 20);
            this.tbLastName.TabIndex = 21;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMiddleName.Location = new System.Drawing.Point(6, 69);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.ReadOnly = true;
            this.tbMiddleName.Size = new System.Drawing.Size(333, 20);
            this.tbMiddleName.TabIndex = 20;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFirstName.Location = new System.Drawing.Point(6, 30);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.ReadOnly = true;
            this.tbFirstName.Size = new System.Drawing.Size(333, 20);
            this.tbFirstName.TabIndex = 19;
            // 
            // gbBookActions
            // 
            this.gbBookActions.Controls.Add(this.backBook);
            this.gbBookActions.Controls.Add(this.getBook);
            this.gbBookActions.Location = new System.Drawing.Point(12, 358);
            this.gbBookActions.Name = "gbBookActions";
            this.gbBookActions.Size = new System.Drawing.Size(348, 57);
            this.gbBookActions.TabIndex = 21;
            this.gbBookActions.TabStop = false;
            this.gbBookActions.Text = "Книги";
            // 
            // backBook
            // 
            this.backBook.Image = ((System.Drawing.Image)(resources.GetObject("backBook.Image")));
            this.backBook.Location = new System.Drawing.Point(116, 19);
            this.backBook.Name = "backBook";
            this.backBook.Size = new System.Drawing.Size(98, 28);
            this.backBook.TabIndex = 1;
            this.backBook.Text = "Сдать книги";
            this.backBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.backBook.UseVisualStyleBackColor = true;
            this.backBook.Click += new System.EventHandler(this.backBook_Click);
            // 
            // getBook
            // 
            this.getBook.Image = ((System.Drawing.Image)(resources.GetObject("getBook.Image")));
            this.getBook.Location = new System.Drawing.Point(9, 19);
            this.getBook.Name = "getBook";
            this.getBook.Size = new System.Drawing.Size(101, 28);
            this.getBook.TabIndex = 0;
            this.getBook.Text = "Выдать книги";
            this.getBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.getBook.UseVisualStyleBackColor = true;
            this.getBook.Click += new System.EventHandler(this.getBook_Click);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column1.DataPropertyName = "exmplr_id";
            this.Column1.FillWeight = 30F;
            this.Column1.HeaderText = "Инв. номер";
            this.Column1.Name = "Column1";
            this.Column1.Width = 97;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "title";
            this.Column2.HeaderText = "Название";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column3.DataPropertyName = "dlv_date";
            this.Column3.FillWeight = 30F;
            this.Column3.HeaderText = "Дата выдачи";
            this.Column3.Name = "Column3";
            this.Column3.Width = 96;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column4.DataPropertyName = "last_day";
            this.Column4.FillWeight = 30F;
            this.Column4.HeaderText = "Выдана до";
            this.Column4.Name = "Column4";
            this.Column4.Width = 97;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(615, 421);
            this.dataGridView2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "exmplr_id";
            this.dataGridViewTextBoxColumn1.FillWeight = 30F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Инв. номер";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 97;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "title";
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "dlv_date";
            this.dataGridViewTextBoxColumn3.FillWeight = 30F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Дата выдачи";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 96;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "last_day";
            this.dataGridViewTextBoxColumn4.FillWeight = 30F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Выдана до";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 97;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(621, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 25);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersVisible = false;
            this.dataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView3.Size = new System.Drawing.Size(621, 402);
            this.dataGridView3.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "exmplr_id";
            this.dataGridViewTextBoxColumn5.FillWeight = 30F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Инв. номер";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 97;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "title";
            this.dataGridViewTextBoxColumn6.HeaderText = "Название";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "dlv_date";
            this.dataGridViewTextBoxColumn7.FillWeight = 30F;
            this.dataGridViewTextBoxColumn7.HeaderText = "Дата выдачи";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 96;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "back_date";
            this.dataGridViewTextBoxColumn8.FillWeight = 30F;
            this.dataGridViewTextBoxColumn8.HeaderText = "Дата возврата";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 97;
            // 
            // frmStudent
            // 
            this.AllowEndUserDocking = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 499);
            this.Controls.Add(this.gbBookActions);
            this.Controls.Add(this.gbCard);
            this.Controls.Add(this.gbStudent);
            this.Controls.Add(this.gbBooks);
            this.DockAreas = WeifenLuo.WinFormsUI.Docking.DockAreas.Document;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "frmStudent";
            this.Text = "Карточка читателя";
            this.gbBooks.ResumeLayout(false);
            this.tcBooks.ResumeLayout(false);
            this.pgOnHands.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pgOutOfDate.ResumeLayout(false);
            this.pgHistory.ResumeLayout(false);
            this.pgHistory.PerformLayout();
            this.gbCard.ResumeLayout(false);
            this.gbCard.PerformLayout();
            this.gbStudent.ResumeLayout(false);
            this.gbStudent.PerformLayout();
            this.gbBookActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbBooks;
        private System.Windows.Forms.GroupBox gbCard;
        private System.Windows.Forms.Label lblEndDate;
        public System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label lblCardNumber;
        public System.Windows.Forms.TextBox tbCardNumber;
        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblBirthDate;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.Label lblFirstName;
        public System.Windows.Forms.TextBox tbPhone;
        public System.Windows.Forms.TextBox tbAddress;
        public System.Windows.Forms.ComboBox cbGender;
        public System.Windows.Forms.DateTimePicker dpBirthDate;
        public System.Windows.Forms.TextBox tbLastName;
        public System.Windows.Forms.TextBox tbMiddleName;
        public System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TabControl tcBooks;
        private System.Windows.Forms.TabPage pgOnHands;
        private System.Windows.Forms.TabPage pgOutOfDate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnChangeCard;
        private System.Windows.Forms.Button btnChangeStudent;
        private System.Windows.Forms.ToolTip toolTipMain;
        private System.Windows.Forms.Button btnViewHistory;
        private System.Windows.Forms.GroupBox gbBookActions;
        private System.Windows.Forms.Button backBook;
        private System.Windows.Forms.Button getBook;
        private System.Windows.Forms.TabPage pgHistory;
        private System.Windows.Forms.Button btnRefreshTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}