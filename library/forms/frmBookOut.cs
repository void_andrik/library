﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmBookOut : frmOkCancel
    {
        public int ppl_id = -1;
        public int crd_id = -1;
        public int book_id = -1;
        List<int> exmplr_ids = new List<int>();

        public frmBookOut(int ppl_id, int crd_id, List<int> exmplr_ids)
        {
            InitializeComponent();
            this.ppl_id = ppl_id;
            this.crd_id = crd_id;
            this.exmplr_ids = exmplr_ids;
            setStudent();
            setBookList();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            frmFindBook fb = new frmFindBook();
            if (fb.ShowDialog() == DialogResult.OK && fb.book_id != -1)
            {
                api.editBook(fb.book_id, true);
                int selectedExmplr_id = api.exmplr_id;
                if (!exmplr_ids.Contains(selectedExmplr_id))
                {
                    exmplr_ids.Add(selectedExmplr_id);
                    setBookList();
                }
            }
        }

        private void btnStudent_Click(object sender, EventArgs e)
        {
            frmFindStudent fs = new frmFindStudent();
            if (fs.ShowDialog() == DialogResult.OK && fs.ppl_id != -1)
            {
                librDataSetTableAdapters.cardsTableAdapter ad = new librDataSetTableAdapters.cardsTableAdapter();
                crd_id = (int)ad.GetCardByPeople(fs.ppl_id);
                ppl_id = fs.ppl_id;
                setStudent();
            }
        }

        private void setStudent()
        {
            librDataSetTableAdapters.studentInfoTableAdapter adapter = new librDataSetTableAdapters.studentInfoTableAdapter();
            librDataSet.studentInfoDataTable table = new librDataSet.studentInfoDataTable();
            adapter.Fill(table, crd_id, ppl_id);
            if (table.Rows.Count == 1)
            {
                string card_num = (string)table.Rows[0]["card_num"];
                string first_name = (string)table.Rows[0]["first_name"];
                string middle_name = (string)table.Rows[0]["middle_name"];
                string last_name = (string)table.Rows[0]["last_name"];
                tbCardNumber.Text = card_num;
                tbFirstName.Text = first_name;
                tbMiddleName.Text = middle_name;
                tbLastName.Text = last_name;
            }
        }

        private void setBookList()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("exmplr_id");
            dt.Columns.Add("title");
            foreach (int i in exmplr_ids)
            {
                librDataSetTableAdapters.exmplrInfoTableAdapter ta = new librDataSetTableAdapters.exmplrInfoTableAdapter();
                librDataSet.exmplrInfoDataTable t = new librDataSet.exmplrInfoDataTable();
                ta.Fill(t, i);
                if (t.Rows.Count > 0)
                {
                    int ex = (int)t.Rows[0]["exmplr_id"];
                    string title = (string)t.Rows[0]["title"];
                    dt.Rows.Add(ex, title);
                }
            }
            gvBooksOut.DataSource = dt;
        }

        private void btnDelBook_Click(object sender, EventArgs e)
        {
            if (gvBooksOut.SelectedRows.Count > 0)
            {
                int selectedExmplr_id = (int)gvBooksOut.SelectedRows[0].Cells["exmplr_id"].Value;
                exmplr_ids.Remove(selectedExmplr_id);
                setBookList();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (ppl_id != -1 && crd_id != -1)
            {
                if (exmplr_ids != null && exmplr_ids.Count > 0)
                {
                    librDataSetTableAdapters.deliveriesTableAdapter adapter = new librDataSetTableAdapters.deliveriesTableAdapter();
                    foreach (int exmplr_id in exmplr_ids)
                        if (exmplr_id != -1)
                            adapter.Insert(exmplr_id, crd_id, DateTime.Now, null, (int?)numDays.Value, Program.Librarian);
                    MessageBox.Show("Выдача произведёна", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else
                    MessageBox.Show("Необходимо выбрать книги для выдачи", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                MessageBox.Show("Необходимо выбрать читателя", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
