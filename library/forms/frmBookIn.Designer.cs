﻿namespace library
{
    partial class frmBookIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBookIn));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gbStudent = new System.Windows.Forms.GroupBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tbMiddleName = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.lblCardNumber = new System.Windows.Forms.Label();
            this.tbCardNumber = new System.Windows.Forms.TextBox();
            this.tsStudent = new System.Windows.Forms.ToolStrip();
            this.btnStudent = new System.Windows.Forms.ToolStripButton();
            this.gbBooksIn = new System.Windows.Forms.GroupBox();
            this.gvBooksIn = new System.Windows.Forms.DataGridView();
            this.colIdIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dlvIn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbBookList = new System.Windows.Forms.GroupBox();
            this.gvBooksOut = new System.Windows.Forms.DataGridView();
            this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDlv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tsBooks = new System.Windows.Forms.ToolStrip();
            this.btnAddBook = new System.Windows.Forms.ToolStripButton();
            this.btnDelBook = new System.Windows.Forms.ToolStripButton();
            this.gbInParams = new System.Windows.Forms.GroupBox();
            this.lblInDate = new System.Windows.Forms.Label();
            this.dpDateIn = new System.Windows.Forms.DateTimePicker();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gbStudent.SuspendLayout();
            this.tsStudent.SuspendLayout();
            this.gbBooksIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksIn)).BeginInit();
            this.gbBookList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksOut)).BeginInit();
            this.tsBooks.SuspendLayout();
            this.gbInParams.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 465);
            this.pnlButtons.Size = new System.Drawing.Size(673, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(592, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(517, 7);
            this.btnOk.Text = "Возврат";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gbInParams);
            this.splitContainer1.Panel1.Controls.Add(this.gbStudent);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gbBooksIn);
            this.splitContainer1.Panel2.Controls.Add(this.gbBookList);
            this.splitContainer1.Size = new System.Drawing.Size(673, 465);
            this.splitContainer1.SplitterDistance = 224;
            this.splitContainer1.TabIndex = 3;
            // 
            // gbStudent
            // 
            this.gbStudent.Controls.Add(this.lblLastName);
            this.gbStudent.Controls.Add(this.lblMiddleName);
            this.gbStudent.Controls.Add(this.lblFirstName);
            this.gbStudent.Controls.Add(this.tbLastName);
            this.gbStudent.Controls.Add(this.tbMiddleName);
            this.gbStudent.Controls.Add(this.tbFirstName);
            this.gbStudent.Controls.Add(this.lblCardNumber);
            this.gbStudent.Controls.Add(this.tbCardNumber);
            this.gbStudent.Controls.Add(this.tsStudent);
            this.gbStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStudent.Location = new System.Drawing.Point(0, 0);
            this.gbStudent.Name = "gbStudent";
            this.gbStudent.Size = new System.Drawing.Size(224, 206);
            this.gbStudent.TabIndex = 2;
            this.gbStudent.TabStop = false;
            this.gbStudent.Text = "Читатель";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(8, 160);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(54, 13);
            this.lblLastName.TabIndex = 32;
            this.lblLastName.Text = "Отчество";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(6, 121);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(29, 13);
            this.lblMiddleName.TabIndex = 31;
            this.lblMiddleName.Text = "Имя";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(6, 82);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(56, 13);
            this.lblFirstName.TabIndex = 30;
            this.lblFirstName.Text = "Фамилия";
            // 
            // tbLastName
            // 
            this.tbLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLastName.Location = new System.Drawing.Point(6, 176);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.ReadOnly = true;
            this.tbLastName.Size = new System.Drawing.Size(210, 20);
            this.tbLastName.TabIndex = 29;
            // 
            // tbMiddleName
            // 
            this.tbMiddleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMiddleName.Location = new System.Drawing.Point(6, 137);
            this.tbMiddleName.Name = "tbMiddleName";
            this.tbMiddleName.ReadOnly = true;
            this.tbMiddleName.Size = new System.Drawing.Size(210, 20);
            this.tbMiddleName.TabIndex = 28;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFirstName.Location = new System.Drawing.Point(6, 98);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.ReadOnly = true;
            this.tbFirstName.Size = new System.Drawing.Size(210, 20);
            this.tbFirstName.TabIndex = 27;
            // 
            // lblCardNumber
            // 
            this.lblCardNumber.AutoSize = true;
            this.lblCardNumber.Location = new System.Drawing.Point(6, 46);
            this.lblCardNumber.Name = "lblCardNumber";
            this.lblCardNumber.Size = new System.Drawing.Size(133, 13);
            this.lblCardNumber.TabIndex = 23;
            this.lblCardNumber.Text = "№ читательского билета";
            // 
            // tbCardNumber
            // 
            this.tbCardNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCardNumber.Location = new System.Drawing.Point(6, 62);
            this.tbCardNumber.Name = "tbCardNumber";
            this.tbCardNumber.ReadOnly = true;
            this.tbCardNumber.Size = new System.Drawing.Size(210, 20);
            this.tbCardNumber.TabIndex = 22;
            // 
            // tsStudent
            // 
            this.tsStudent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnStudent});
            this.tsStudent.Location = new System.Drawing.Point(3, 16);
            this.tsStudent.Name = "tsStudent";
            this.tsStudent.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsStudent.Size = new System.Drawing.Size(218, 25);
            this.tsStudent.TabIndex = 0;
            this.tsStudent.Text = "toolStrip2";
            // 
            // btnStudent
            // 
            this.btnStudent.Image = ((System.Drawing.Image)(resources.GetObject("btnStudent.Image")));
            this.btnStudent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStudent.Name = "btnStudent";
            this.btnStudent.Size = new System.Drawing.Size(122, 22);
            this.btnStudent.Text = "Выбрать читателя";
            this.btnStudent.Click += new System.EventHandler(this.btnStudent_Click);
            // 
            // gbBooksIn
            // 
            this.gbBooksIn.Controls.Add(this.gvBooksIn);
            this.gbBooksIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbBooksIn.Location = new System.Drawing.Point(0, 235);
            this.gbBooksIn.Name = "gbBooksIn";
            this.gbBooksIn.Size = new System.Drawing.Size(445, 230);
            this.gbBooksIn.TabIndex = 1;
            this.gbBooksIn.TabStop = false;
            this.gbBooksIn.Text = "Список книг к возврату";
            // 
            // gvBooksIn
            // 
            this.gvBooksIn.AllowUserToAddRows = false;
            this.gvBooksIn.AllowUserToDeleteRows = false;
            this.gvBooksIn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksIn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvBooksIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvBooksIn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIdIn,
            this.dataGridViewTextBoxColumn2,
            this.dlvIn});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvBooksIn.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvBooksIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvBooksIn.Location = new System.Drawing.Point(3, 16);
            this.gvBooksIn.Name = "gvBooksIn";
            this.gvBooksIn.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksIn.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvBooksIn.RowHeadersVisible = false;
            this.gvBooksIn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvBooksIn.Size = new System.Drawing.Size(439, 211);
            this.gvBooksIn.TabIndex = 1;
            this.gvBooksIn.DoubleClick += new System.EventHandler(this.btnDelBook_Click);
            // 
            // colIdIn
            // 
            this.colIdIn.DataPropertyName = "exmplr_id";
            this.colIdIn.FillWeight = 30F;
            this.colIdIn.HeaderText = "Инв. номер";
            this.colIdIn.Name = "colIdIn";
            this.colIdIn.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "title";
            this.dataGridViewTextBoxColumn2.HeaderText = "Название";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dlvIn
            // 
            this.dlvIn.DataPropertyName = "dlv_id";
            this.dlvIn.HeaderText = "dlv_id";
            this.dlvIn.Name = "dlvIn";
            this.dlvIn.ReadOnly = true;
            this.dlvIn.Visible = false;
            // 
            // gbBookList
            // 
            this.gbBookList.Controls.Add(this.gvBooksOut);
            this.gbBookList.Controls.Add(this.tsBooks);
            this.gbBookList.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbBookList.Location = new System.Drawing.Point(0, 0);
            this.gbBookList.Name = "gbBookList";
            this.gbBookList.Size = new System.Drawing.Size(445, 235);
            this.gbBookList.TabIndex = 0;
            this.gbBookList.TabStop = false;
            this.gbBookList.Text = "Список книг";
            // 
            // gvBooksOut
            // 
            this.gvBooksOut.AllowUserToAddRows = false;
            this.gvBooksOut.AllowUserToDeleteRows = false;
            this.gvBooksOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksOut.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.gvBooksOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvBooksOut.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colTitle,
            this.colDlv});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvBooksOut.DefaultCellStyle = dataGridViewCellStyle5;
            this.gvBooksOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvBooksOut.Location = new System.Drawing.Point(3, 41);
            this.gvBooksOut.Name = "gvBooksOut";
            this.gvBooksOut.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvBooksOut.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.gvBooksOut.RowHeadersVisible = false;
            this.gvBooksOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvBooksOut.Size = new System.Drawing.Size(439, 191);
            this.gvBooksOut.TabIndex = 1;
            this.gvBooksOut.DoubleClick += new System.EventHandler(this.btnAddBook_Click);
            // 
            // colID
            // 
            this.colID.DataPropertyName = "exmplr_id";
            this.colID.FillWeight = 30F;
            this.colID.HeaderText = "Инв. номер";
            this.colID.Name = "colID";
            this.colID.ReadOnly = true;
            // 
            // colTitle
            // 
            this.colTitle.DataPropertyName = "title";
            this.colTitle.HeaderText = "Название";
            this.colTitle.Name = "colTitle";
            this.colTitle.ReadOnly = true;
            // 
            // colDlv
            // 
            this.colDlv.DataPropertyName = "dlv_id";
            this.colDlv.HeaderText = "colDlv";
            this.colDlv.Name = "colDlv";
            this.colDlv.ReadOnly = true;
            this.colDlv.Visible = false;
            // 
            // tsBooks
            // 
            this.tsBooks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddBook,
            this.btnDelBook});
            this.tsBooks.Location = new System.Drawing.Point(3, 16);
            this.tsBooks.Name = "tsBooks";
            this.tsBooks.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsBooks.Size = new System.Drawing.Size(439, 25);
            this.tsBooks.TabIndex = 0;
            this.tsBooks.Text = "toolStrip1";
            // 
            // btnAddBook
            // 
            this.btnAddBook.Image = ((System.Drawing.Image)(resources.GetObject("btnAddBook.Image")));
            this.btnAddBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(136, 22);
            this.btnAddBook.Text = "Добавить к возврату";
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // btnDelBook
            // 
            this.btnDelBook.Image = ((System.Drawing.Image)(resources.GetObject("btnDelBook.Image")));
            this.btnDelBook.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelBook.Name = "btnDelBook";
            this.btnDelBook.Size = new System.Drawing.Size(135, 22);
            this.btnDelBook.Text = "Удалить из возврата";
            this.btnDelBook.Click += new System.EventHandler(this.btnDelBook_Click);
            // 
            // gbInParams
            // 
            this.gbInParams.Controls.Add(this.dpDateIn);
            this.gbInParams.Controls.Add(this.lblInDate);
            this.gbInParams.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbInParams.Location = new System.Drawing.Point(0, 206);
            this.gbInParams.Name = "gbInParams";
            this.gbInParams.Size = new System.Drawing.Size(224, 64);
            this.gbInParams.TabIndex = 4;
            this.gbInParams.TabStop = false;
            this.gbInParams.Text = "Выдача";
            // 
            // lblInDate
            // 
            this.lblInDate.AutoSize = true;
            this.lblInDate.Location = new System.Drawing.Point(6, 16);
            this.lblInDate.Name = "lblInDate";
            this.lblInDate.Size = new System.Drawing.Size(83, 13);
            this.lblInDate.TabIndex = 33;
            this.lblInDate.Text = "Дата возврата";
            // 
            // dpDateIn
            // 
            this.dpDateIn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dpDateIn.Location = new System.Drawing.Point(6, 32);
            this.dpDateIn.Name = "dpDateIn";
            this.dpDateIn.Size = new System.Drawing.Size(210, 20);
            this.dpDateIn.TabIndex = 34;
            // 
            // frmBookIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 508);
            this.Controls.Add(this.splitContainer1);
            this.Name = "frmBookIn";
            this.Text = "Возврат книг";
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.pnlButtons.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gbStudent.ResumeLayout(false);
            this.gbStudent.PerformLayout();
            this.tsStudent.ResumeLayout(false);
            this.tsStudent.PerformLayout();
            this.gbBooksIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksIn)).EndInit();
            this.gbBookList.ResumeLayout(false);
            this.gbBookList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvBooksOut)).EndInit();
            this.tsBooks.ResumeLayout(false);
            this.tsBooks.PerformLayout();
            this.gbInParams.ResumeLayout(false);
            this.gbInParams.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox gbStudent;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private System.Windows.Forms.Label lblFirstName;
        public System.Windows.Forms.TextBox tbLastName;
        public System.Windows.Forms.TextBox tbMiddleName;
        public System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label lblCardNumber;
        public System.Windows.Forms.TextBox tbCardNumber;
        private System.Windows.Forms.ToolStrip tsStudent;
        private System.Windows.Forms.ToolStripButton btnStudent;
        private System.Windows.Forms.GroupBox gbBookList;
        private System.Windows.Forms.DataGridView gvBooksOut;
        private System.Windows.Forms.ToolStrip tsBooks;
        private System.Windows.Forms.ToolStripButton btnAddBook;
        private System.Windows.Forms.ToolStripButton btnDelBook;
        private System.Windows.Forms.GroupBox gbBooksIn;
        private System.Windows.Forms.DataGridView gvBooksIn;
        private System.Windows.Forms.DataGridViewTextBoxColumn colID;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDlv;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdIn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dlvIn;
        private System.Windows.Forms.GroupBox gbInParams;
        private System.Windows.Forms.Label lblInDate;
        private System.Windows.Forms.DateTimePicker dpDateIn;
    }
}