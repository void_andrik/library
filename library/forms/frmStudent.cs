﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace library
{
    public partial class frmStudent : DockContent
    {
        int _ppl_id = -1;
        int _crd_id = -1;
        DateTimePicker dp_from = new DateTimePicker();
        DateTimePicker dp_to = new DateTimePicker();

        public frmStudent(int ppl_id, int crd_id)
        {
            InitializeComponent();
            this._ppl_id = ppl_id;
            this._crd_id = crd_id;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView2.AutoGenerateColumns = false;
            dataGridView3.AutoGenerateColumns = false;
            ToolStripControlHost ch1 = new ToolStripControlHost(dp_from);
            ToolStripControlHost ch2 = new ToolStripControlHost(dp_to);
            dp_from.Width = 127;
            dp_to.Width = 127;
            dp_from.Value = DateTime.Now.AddDays(-30);
            dp_to.Value = DateTime.Now;
            toolStrip1.Items.Add(ch1);
            toolStrip1.Items.Add(ch2);
            setInfo();
            loadTable();
        }

        public void setInfo()
        {
            librDataSetTableAdapters.studentInfoTableAdapter adapter = new librDataSetTableAdapters.studentInfoTableAdapter();
            librDataSet.studentInfoDataTable table = new librDataSet.studentInfoDataTable();
            adapter.Fill(table, this._crd_id, this._ppl_id);
            if (table.Rows.Count == 1)
            {
                string card_num = (string)table.Rows[0]["card_num"];
                DateTime navi_date = (DateTime)table.Rows[0]["navi_date"];
                DateTime upd_date = (DateTime)table.Rows[0]["upd_date"];
                DateTime end_date = (DateTime)table.Rows[0]["end_date"];
                string first_name = (string)table.Rows[0]["first_name"];
                string middle_name = (string)table.Rows[0]["middle_name"];
                string last_name = (string)table.Rows[0]["last_name"];
                string address = table.Rows[0]["address"].ToString();
                string phone = table.Rows[0]["phone"].ToString();
                string gender = (string)table.Rows[0]["gender"];
                string bDate = table.Rows[0]["birth_date"].ToString();
                DateTime birthDate = DateTime.Now;
                DateTime.TryParse(bDate, out birthDate);
                string title = string.Format("{0} {1}.{2}.", first_name, middle_name.Substring(0, 1), last_name.Substring(0, 1));
                this.Text = title;
                tbCardNumber.Text = card_num;
                dpEndDate.Value = end_date;
                tbFirstName.Text = first_name;
                tbMiddleName.Text = middle_name;
                tbLastName.Text = last_name;
                tbAddress.Text = address;
                tbPhone.Text = phone;
                cbGender.Text = gender;
                dpBirthDate.Value = birthDate;
            }
        }

        private void btnChangeStudent_Click(object sender, EventArgs e)
        {
            if (api.editStudent(this._ppl_id, this._crd_id))
                setInfo();
        }

        private void btnChangeCard_Click(object sender, EventArgs e)
        {
            frmChangeCard cc = new frmChangeCard();
            cc.tbOldCardNumber.Text = tbCardNumber.Text;
            if (cc.ShowDialog() == DialogResult.OK)
            {
                this._crd_id = api.changeCard(this._ppl_id, this._crd_id, cc.tbCardNumber.Text, cc.dpEndDate.Value);
                setInfo();
            }
        }

        private void getBook_Click(object sender, EventArgs e)
        {
            api.bookOut(_ppl_id, _crd_id);
            this.Close();
        }

        private void backBook_Click(object sender, EventArgs e)
        {
            api.bookIn(_ppl_id, _crd_id);
            this.Close();
        }

        private void tcBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadTable();
        }

        public void loadTable()
        {
            if (tcBooks.SelectedTab == pgOnHands)
            {
                librDataSetTableAdapters.booksOutTableAdapter adapter = new librDataSetTableAdapters.booksOutTableAdapter();
                librDataSet.booksOutDataTable table = new librDataSet.booksOutDataTable();
                adapter.Fill(table, _ppl_id);
                dataGridView1.DataSource = table;
            }
            else if (tcBooks.SelectedTab == pgOutOfDate)
            {
                librDataSetTableAdapters.booksOutTableAdapter adapter = new librDataSetTableAdapters.booksOutTableAdapter();
                librDataSet.booksOutDataTable table = new librDataSet.booksOutDataTable();
                string s = DateTime.Now.ToShortDateString();
                adapter.FillByOutOfDate(table, _ppl_id, s);
                dataGridView2.DataSource = table;
            }
            else if (tcBooks.SelectedTab == pgHistory)
            {
                librDataSetTableAdapters.historyTableAdapter adapter = new librDataSetTableAdapters.historyTableAdapter();
                librDataSet.historyDataTable table = new librDataSet.historyDataTable();
                adapter.Fill(table, _ppl_id, dp_from.Value, dp_to.Value);
                dataGridView3.DataSource = table;
            }
        }

        private void btnRefreshTable_Click(object sender, EventArgs e)
        {
            loadTable();
        }

    }
}
