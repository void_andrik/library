﻿namespace library
{
    partial class frmBookReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.rep1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.librDataSet = new library.librDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.rep1TableAdapter = new library.librDataSetTableAdapters.rep1TableAdapter();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rep1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.Location = new System.Drawing.Point(0, 554);
            this.pnlButtons.Size = new System.Drawing.Size(797, 43);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(716, 7);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(641, 7);
            // 
            // rep1BindingSource
            // 
            this.rep1BindingSource.DataMember = "rep1";
            this.rep1BindingSource.DataSource = this.librDataSet;
            // 
            // librDataSet
            // 
            this.librDataSet.DataSetName = "librDataSet";
            this.librDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "rep1DS";
            reportDataSource1.Value = this.rep1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "library.reports.repOnHands.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(797, 554);
            this.reportViewer1.TabIndex = 1;
            // 
            // rep1TableAdapter
            // 
            this.rep1TableAdapter.ClearBeforeFill = true;
            // 
            // frmBookReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 597);
            this.Controls.Add(this.reportViewer1);
            this.Name = "frmBookReport";
            this.Text = "Отчёт";
            this.Load += new System.EventHandler(this.frmBookReport_Load);
            this.Controls.SetChildIndex(this.pnlButtons, 0);
            this.Controls.SetChildIndex(this.reportViewer1, 0);
            this.pnlButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rep1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.librDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource rep1BindingSource;
        private librDataSet librDataSet;
        private librDataSetTableAdapters.rep1TableAdapter rep1TableAdapter;

    }
}