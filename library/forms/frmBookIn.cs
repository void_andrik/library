﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmBookIn : frmOkCancel
    {
        public int ppl_id = -1;
        public int crd_id = -1;
        public int book_id = -1;
        List<int> exmplr_ids = new List<int>();

        public frmBookIn(int ppl_id, int crd_id, List<int> exmplr_ids)
        {
            InitializeComponent();
            this.ppl_id = ppl_id;
            this.crd_id = crd_id;
            this.exmplr_ids = exmplr_ids;
            gvBooksIn.AutoGenerateColumns = false;
            dpDateIn.Value = DateTime.Now;
            setStudent();
            setBooks();
        }

        private void setStudent()
        {
            librDataSetTableAdapters.studentInfoTableAdapter adapter = new librDataSetTableAdapters.studentInfoTableAdapter();
            librDataSet.studentInfoDataTable table = new librDataSet.studentInfoDataTable();
            adapter.Fill(table, crd_id, ppl_id);
            if (table.Rows.Count == 1)
            {
                string card_num = (string)table.Rows[0]["card_num"];
                string first_name = (string)table.Rows[0]["first_name"];
                string middle_name = (string)table.Rows[0]["middle_name"];
                string last_name = (string)table.Rows[0]["last_name"];
                tbCardNumber.Text = card_num;
                tbFirstName.Text = first_name;
                tbMiddleName.Text = middle_name;
                tbLastName.Text = last_name;
            }
        }

        public void setBooks()
        {
            librDataSetTableAdapters.booksOutTableAdapter adapter = new librDataSetTableAdapters.booksOutTableAdapter();
            librDataSet.booksOutDataTable table = new librDataSet.booksOutDataTable();
            adapter.Fill(table, ppl_id);
            gvBooksOut.AutoGenerateColumns = false;
            gvBooksOut.DataSource = table;

            DataTable dt = new DataTable();
            dt.Columns.Add("exmplr_id");
            dt.Columns.Add("title");
            dt.Columns.Add("dlv_id");
            foreach (int i in exmplr_ids)
            {
                librDataSetTableAdapters.exmplrInfoTableAdapter ta = new librDataSetTableAdapters.exmplrInfoTableAdapter();
                librDataSet.exmplrInfoDataTable t = new librDataSet.exmplrInfoDataTable();
                ta.Fill(t, i);
                int ex = (int)t.Rows[0]["exmplr_id"];
                string title = (string)t.Rows[0]["title"];
                string dlv_id = t.Rows[0]["dlv_id"].ToString();
                dt.Rows.Add(ex, title, dlv_id);
            }
            gvBooksIn.DataSource = dt;

        }

        private void btnStudent_Click(object sender, EventArgs e)
        {
            frmFindStudent fs = new frmFindStudent();
            if (fs.ShowDialog() == DialogResult.OK && fs.ppl_id != -1)
            {
                librDataSetTableAdapters.cardsTableAdapter ad = new librDataSetTableAdapters.cardsTableAdapter();
                crd_id = (int)ad.GetCardByPeople(fs.ppl_id);
                ppl_id = fs.ppl_id;
                setStudent();
                setBooks();
            }
        }

        private void btnAddBook_Click(object sender, EventArgs e)
        {
            if (gvBooksOut.SelectedRows.Count > 0)
            {
                int selectedExmplr_id = (int)gvBooksOut.SelectedRows[0].Cells["colID"].Value;
                if (!exmplr_ids.Contains(selectedExmplr_id))
                    exmplr_ids.Add(selectedExmplr_id);
                setBooks();
            }
        }

        private void btnDelBook_Click(object sender, EventArgs e)
        {
            if (gvBooksIn.SelectedRows.Count > 0)
            {
                int selectedExmplr_id = int.Parse(gvBooksIn.SelectedRows[0].Cells["colIdIn"].Value.ToString());
                exmplr_ids.Remove(selectedExmplr_id);
                setBooks();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (ppl_id != -1 && crd_id != -1)
            {
                if (exmplr_ids != null && exmplr_ids.Count > 0)
                {
                    librDataSetTableAdapters.deliveriesTableAdapter adapter = new librDataSetTableAdapters.deliveriesTableAdapter();
                    for (int i = 0; i < gvBooksIn.RowCount; i++)
                    {
                        int dlv_id = int.Parse(gvBooksIn.Rows[i].Cells["dlvIn"].Value.ToString());
                        adapter.closeDlv(dpDateIn.Value, dlv_id);
                    }
                    MessageBox.Show("Возврат произведён", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                else
                    MessageBox.Show("Необходимо выбрать книги для сдачи", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                MessageBox.Show("Необходимо выбрать читателя", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

        }
    }
}
