﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace library
{
    public static class extensions
    {
        //Пробовал заменять в дизайнере - слетает при любом изменении в дизайнере. Вынес в Extension методы
        public static int InsertAndGetID(this librDataSetTableAdapters.peoplesTableAdapter adapter, global::System.Nullable<int> grp_grp_id, string first_name, string last_name, global::System.Nullable<global::System.DateTime> birth_date, string gender, string address, string phone, string middle_name)
        {
            if ((grp_grp_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = ((int)(grp_grp_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
            }
            if ((first_name == null))
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = ((string)(first_name));
            }
            if ((middle_name == null))
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = ((string)(middle_name));
            }
            if ((last_name == null))
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = ((string)(last_name));
            }
            if ((birth_date.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = ((System.DateTime)(birth_date.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
            }
            if ((gender == null))
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = ((string)(gender));
            }
            if ((address == null))
            {
                adapter.Adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[6].Value = ((string)(address));
            }
            if ((phone == null))
            {
                adapter.Adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[7].Value = ((string)(phone));
            }
            global::System.Data.ConnectionState previousConnectionState = adapter.Adapter.InsertCommand.Connection.State;
            if (((adapter.Adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                        != global::System.Data.ConnectionState.Open))
            {
                adapter.Adapter.InsertCommand.Connection.Open();
            }
            try
            {
                /*Немного переписали инсерт*/
                adapter.Adapter.InsertCommand.ExecuteNonQuery();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = adapter.Connection;
                cmd.CommandText = "SELECT @@IDENTITY";
                int returnValue = (int)cmd.ExecuteScalar();
                return returnValue;
            }
            finally
            {
                if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                {
                    adapter.Adapter.InsertCommand.Connection.Close();
                }
            }
        }

        public static int InsertAndGetID(this librDataSetTableAdapters.cardsTableAdapter adapter, global::System.Nullable<int> ppl_ppl_id, string card_num, global::System.Nullable<global::System.DateTime> navi_date, global::System.Nullable<global::System.DateTime> upd_date, global::System.Nullable<global::System.DateTime> end_date, global::System.Nullable<int> crs_crs_id)
        {
            if ((ppl_ppl_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = ((int)(ppl_ppl_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
            }
            if ((card_num == null))
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = ((string)(card_num));
            }
            if ((navi_date.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = ((System.DateTime)(navi_date.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
            }
            if ((upd_date.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = ((System.DateTime)(upd_date.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
            }
            if ((end_date.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = ((System.DateTime)(end_date.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
            }
            if ((crs_crs_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = ((int)(crs_crs_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
            }
            global::System.Data.ConnectionState previousConnectionState = adapter.Adapter.InsertCommand.Connection.State;
            if (((adapter.Adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                        != global::System.Data.ConnectionState.Open))
            {
                adapter.Adapter.InsertCommand.Connection.Open();
            }
            try
            {
                adapter.Adapter.InsertCommand.ExecuteNonQuery();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = adapter.Connection;
                cmd.CommandText = "SELECT @@IDENTITY";
                int returnValue = (int)cmd.ExecuteScalar();
                return returnValue;
            }
            finally
            {
                if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                {
                    adapter.Adapter.InsertCommand.Connection.Close();
                }
            }
        }

        public static int InsertAndGetID(this librDataSetTableAdapters.booksTableAdapter adapter, string title, global::System.Nullable<int> pub_pub_id, string isbn, global::System.Nullable<global::System.DateTime> book_year, global::System.Nullable<int> pages, string bbk, string a_znak, global::System.Nullable<int> vol, global::System.Nullable<int> ver, global::System.Nullable<int> vid_vid_id, global::System.Nullable<int> typ_typ_id, string annotation)
        {
            if ((title == null))
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = ((string)(title));
            }
            if ((pub_pub_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = ((int)(pub_pub_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
            }
            if ((isbn == null))
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = ((string)(isbn));
            }
            if ((book_year.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = ((System.DateTime)(book_year.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
            }
            if ((pages.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = ((int)(pages.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
            }
            if ((bbk == null))
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[5].Value = ((string)(bbk));
            }
            if ((a_znak == null))
            {
                adapter.Adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[6].Value = ((string)(a_znak));
            }
            if ((vol.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[7].Value = ((int)(vol.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
            }
            if ((ver.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[8].Value = ((int)(ver.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[8].Value = global::System.DBNull.Value;
            }
            if ((vid_vid_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[9].Value = ((int)(vid_vid_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[9].Value = global::System.DBNull.Value;
            }
            if ((typ_typ_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[10].Value = ((int)(typ_typ_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
            }
            if ((annotation == null))
            {
                adapter.Adapter.InsertCommand.Parameters[11].Value = global::System.DBNull.Value;
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[11].Value = ((string)(annotation));
            }
            global::System.Data.ConnectionState previousConnectionState = adapter.Adapter.InsertCommand.Connection.State;
            if (((adapter.Adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                        != global::System.Data.ConnectionState.Open))
            {
                adapter.Adapter.InsertCommand.Connection.Open();
            }
            try
            {
                adapter.Adapter.InsertCommand.ExecuteNonQuery();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = adapter.Connection;
                cmd.CommandText = "SELECT @@IDENTITY";
                int returnValue = (int)cmd.ExecuteScalar();
                return returnValue;
            }
            finally
            {
                if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                {
                    adapter.Adapter.InsertCommand.Connection.Close();
                }
            }
        }

        public static int InsertAndGetID(this librDataSetTableAdapters.membersTableAdapter adapter, global::System.Nullable<int> book_book_id, global::System.Nullable<int> ppl_ppl_id, global::System.Nullable<int> mtyp_mtyp_id)
        {
            if ((book_book_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = ((int)(book_book_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
            }
            if ((ppl_ppl_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = ((int)(ppl_ppl_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
            }
            if ((mtyp_mtyp_id.HasValue == true))
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = ((int)(mtyp_mtyp_id.Value));
            }
            else
            {
                adapter.Adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
            }
            global::System.Data.ConnectionState previousConnectionState = adapter.Adapter.InsertCommand.Connection.State;
            if (((adapter.Adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                        != global::System.Data.ConnectionState.Open))
            {
                adapter.Adapter.InsertCommand.Connection.Open();
            }
            try
            {
                adapter.Adapter.InsertCommand.ExecuteNonQuery();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = adapter.Connection;
                cmd.CommandText = "SELECT @@IDENTITY";
                int returnValue = (int)cmd.ExecuteScalar();
                return returnValue;
            }
            finally
            {
                if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                {
                    adapter.Adapter.InsertCommand.Connection.Close();
                }
            }
        }

    }
}
