﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace library
{
    public partial class frmTableEditor : frmOkCancel
    {
        public BindingSource bs = new BindingSource();
        public string filterText = "";
        int SelectedRowIndex;
        int FirstDisplayedScrollingRowIndex;

        public frmTableEditor()
        {
            InitializeComponent();
            gridEditor.DataSource = bs;
        }

        private void tbFilter_TextChanged(object sender, EventArgs e)
        {
            if (filterText.Length > 0 && tbFilter.Text.Length > 0)
                bs.Filter = string.Format(filterText, tbFilter.Text);
            else
                bs.Filter = "";
        }

        private void gridEditor_DoubleClick(object sender, EventArgs e)
        {
            btnEdit.PerformClick();
        }

        public void rememberRow()
        {
            FirstDisplayedScrollingRowIndex = this.gridEditor.FirstDisplayedScrollingRowIndex; //Save Current Scroll Index
            SelectedRowIndex = 0;
            if (this.gridEditor.SelectedRows.Count > 0) SelectedRowIndex = this.gridEditor.SelectedRows[0].Index; //Save Current Selected Row Index

        }

        public void restoreRow()
        {
            if ((FirstDisplayedScrollingRowIndex >= 0) && ((this.gridEditor.Rows.Count - 1) >= FirstDisplayedScrollingRowIndex)) this.gridEditor.FirstDisplayedScrollingRowIndex = FirstDisplayedScrollingRowIndex; //Restore Scroll Index
            if ((this.gridEditor.Rows.Count - 1) >= SelectedRowIndex) this.gridEditor.Rows[SelectedRowIndex].Selected = true; //Restore Selected Row
        }

    }
}
