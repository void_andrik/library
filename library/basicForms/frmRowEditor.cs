﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace library
{
    public partial class frmRowEditor : frmOkCancel
    {
        Dictionary<string, string> _items;
        Dictionary<string, string> _item_def;
        Dictionary<string, string> _item_val;

        public frmRowEditor(Dictionary<string, string> items, Dictionary<string, string> item_def = null, Dictionary<string, string> item_val = null)
        {
            InitializeComponent();
            _items = items;
            _item_def = item_def;
            _item_val = item_val;
            int lastTop = 2;
            foreach (KeyValuePair<string, string> kvp in _items)
            {
                Label lbl = new Label();
                if (_item_def != null && _item_def.ContainsKey(kvp.Key))
                    lbl.Text = _item_def[kvp.Key];
                else
                    lbl.Text = kvp.Key;
                lbl.Left = 2;
                lbl.Height = 18;
                lbl.AutoSize = true;
                lbl.Top = lastTop;
                lastTop += lbl.Height + 2;
                pnlControls.Controls.Add(lbl);
                if (kvp.Value == "TXT" || kvp.Value=="ID")
                {
                    TextBox tb = new TextBox();
                    tb.Width = pnlControls.Width - 4;
                    tb.Left = 2;
                    tb.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                    tb.Top = lastTop;
                    lastTop += tb.Height + 5;
                    if (_item_val!=null && _item_val.ContainsKey(kvp.Key))
                        tb.Text = _item_val[kvp.Key];
                    if (kvp.Value == "ID")
                        tb.ReadOnly = true;
                    tb.Name = "tb_" + kvp.Key;
                    pnlControls.Controls.Add(tb);
                }
                else if (kvp.Value == "PCK")
                {
                    TextBox tb = new TextBox();
                    tb.Width = pnlControls.Width - 4 - 27 - 2;
                    tb.Left = 2;
                    tb.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                    tb.Top = lastTop;
                    tb.ReadOnly = true;
                    tb.Tag = -1;
                    tb.Name = "tb_" + kvp.Key;

                    Button btn = new Button();
                    btn.Width = 27;
                    btn.Height = tb.Height;
                    btn.Left = tb.Left + tb.Width + 2;
                    btn.Anchor = (AnchorStyles.Top | AnchorStyles.Right);
                    btn.Top = lastTop;
                    btn.Text = "...";
                    //btn.FlatStyle = FlatStyle.Flat;
                    btn.Tag = kvp.Key;
                    btn.Click += new EventHandler(btn_Click);

                    lastTop += tb.Height + 5;
                    pnlControls.Controls.Add(tb);
                    pnlControls.Controls.Add(btn);
                }
                else if (kvp.Value == "DAT" || kvp.Value == "NULL_DAT")
                {
                    DateTimePicker dp = new DateTimePicker();
                    int shft = 0;
                    if (kvp.Value == "NULL_DAT")
                    {
                        CheckBox cb = new CheckBox();
                        cb.Checked = false;
                        cb.Left = 2;
                        cb.Width = 15;
                        cb.Top = lastTop - 2;
                        cb.Name = "cb_" + kvp.Key;
                        dp.Enabled = false;
                        cb.Tag = dp;
                        shft = 20;
                        cb.CheckedChanged += new EventHandler(cb_CheckedChanged);
                        pnlControls.Controls.Add(cb);
                        if (_item_val != null && _item_val.ContainsKey(kvp.Key))
                        {
                            string val = _item_val[kvp.Key];
                            if (val != "")
                            {
                                dp.Value = DateTime.Parse(_item_val[kvp.Key]);
                                dp.Enabled = true;
                                cb.Checked = true;
                            }
                            else
                            {
                                dp.Value = DateTime.Now;
                                dp.Enabled = false;
                                cb.Checked = false;
                            }
                        }
                    }
                    dp.Width = pnlControls.Width - 4 - shft;
                    dp.Left = 2 + shft;
                    dp.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                    dp.Top = lastTop;
                    dp.Name = "tb_" + kvp.Key;
                    dp.CustomFormat = "dd.mm.yyyy";
                    dp.Format = DateTimePickerFormat.Short;
                    lastTop += dp.Height + 5;
                    pnlControls.Controls.Add(dp);
                }
                else if (kvp.Value == "INT")
                {
                    NumericUpDown num = new NumericUpDown();
                    num.Minimum = 0;
                    num.Maximum = 99999999;
                    num.Width = pnlControls.Width - 4;
                    num.Left = 2;
                    num.Anchor = (AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
                    num.Top = lastTop;
                    num.Name = "tb_" + kvp.Key;
                    if (_item_val != null && _item_val.ContainsKey(kvp.Key))
                        num.Value = int.Parse(_item_val[kvp.Key]);
                    lastTop += num.Height + 5;
                    pnlControls.Controls.Add(num);
                }
            }
            this.Height = lastTop + pnlButtons.Height + 30;
        }

        void cb_CheckedChanged(object sender, EventArgs e)
        {
            ((sender as CheckBox).Tag as DateTimePicker).Enabled = (sender as CheckBox).Checked;
        }

        public string getValue(string ctrl)
        {
            object ob = pnlControls.Controls["tb_"+ctrl];
            if (ob != null)
            {
                if (ob.GetType() == typeof(TextBox))
                    return (ob as TextBox).Text;
                else if (ob.GetType() == typeof(DateTimePicker))
                    if ((ob as DateTimePicker).Enabled)
                        return (ob as DateTimePicker).Value.ToShortDateString();
                    else
                        return "";
                else if (ob.GetType() == typeof(NumericUpDown))
                    return (ob as NumericUpDown).Value.ToString();
                else
                    return "";
            }
            else
                return "";
        }

        public int getValueID(string ctrl)
        {
            object ob = pnlControls.Controls["tb_" + ctrl];
            if (ob != null)
                return (int)(ob as TextBox).Tag;
            else
                return -1;
        }

        void btn_Click(object sender, EventArgs e)
        {
            string dict = (sender as Button).Tag.ToString();
            int id = -1;
            string def = "";
            if (dict == "mtyp_id")
            {
                frmMemberTypes bt = new frmMemberTypes();
                if (bt.ShowDialog() == DialogResult.OK && bt.gridEditor.SelectedRows.Count==1)
                {
                    id = (int)bt.gridEditor.SelectedRows[0].Cells[0].Value;
                    def = bt.gridEditor.SelectedRows[0].Cells[1].Value.ToString();
                }
            }
            else if (dict == "ppl_id")
            {
                frmMembers mm = new frmMembers();
                if (mm.ShowDialog() == DialogResult.OK && mm.gridEditor.SelectedRows.Count == 1)
                {
                    id = (int)mm.gridEditor.SelectedRows[0].Cells[0].Value;
                    def = mm.gridEditor.SelectedRows[0].Cells[1].Value.ToString() +" "+ mm.gridEditor.SelectedRows[0].Cells[2].Value.ToString().Substring(0,1)+". " + mm.gridEditor.SelectedRows[0].Cells[3].Value.ToString().Substring(0,1)+".";
                }
            }
            object ob = pnlControls.Controls["tb_" + dict];
            if (ob != null)
            {
                (ob as TextBox).Tag = id;
                (ob as TextBox).Text = def;
            }

        }
    }
}
