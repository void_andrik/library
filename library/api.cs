﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeifenLuo.WinFormsUI.Docking;
using System.Windows.Forms;

namespace library
{
    public static class api
    {
        public static int exmplr_id = -1;

        /// <summary>
        /// Замена читательского билета
        /// </summary>
        /// <param name="ppl_id">ID читателя</param>
        /// <param name="old_crd_id">ID старого билета</param>
        /// <param name="newCardNumber">Номер нового билета</param>
        /// <param name="endDate">Срок действия нового билета</param>
        /// <returns>ID нового билета</returns>
        public static int changeCard(int ppl_id, int old_crd_id, string newCardNumber, DateTime endDate)
        {
            librDataSetTableAdapters.cardsTableAdapter adapter = new librDataSetTableAdapters.cardsTableAdapter();
            adapter.closeCard(old_crd_id);
            int new_crd_id = adapter.InsertAndGetID(ppl_id, newCardNumber, DateTime.Now, DateTime.Now, endDate, 1);
            return new_crd_id;
        }

        /// <summary>
        /// Добавить читателя
        /// </summary>
        /// <param name="openAfterCreate">Открыть ли карточки после создания</param>
        /// <param name="pnl">В какую панель отобразить при открытии</param>
        public static void addStudent(bool openAfterCreate = false)
        {
            frmEditStudent ns = new frmEditStudent(true);
            if (ns.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string cardNumber = ns.tbCardNumber.Text;
                string firstName = ns.tbFirstName.Text;
                string middleName = ns.tbMiddleName.Text;
                string lastName = ns.tbLastName.Text;
                DateTime birthDate = ns.dpBirthDate.Value;
                string gender = ns.cbGender.Text;
                string address = ns.tbAddress.Text;
                string phone = ns.tbPhone.Text;
                DateTime endDate = ns.dpEndDate.Value;

                librDataSetTableAdapters.peoplesTableAdapter pepAdapter = new librDataSetTableAdapters.peoplesTableAdapter();
                librDataSetTableAdapters.cardsTableAdapter cardAdapter = new librDataSetTableAdapters.cardsTableAdapter();
                int new_ppl_id = pepAdapter.InsertAndGetID(1, firstName, lastName, birthDate, gender, address, phone, middleName);
                int new_crd_id = cardAdapter.InsertAndGetID(new_ppl_id, cardNumber, DateTime.Now, DateTime.Now, endDate, 1);

                if (openAfterCreate)
                    openStudent(new_ppl_id, new_crd_id);
            }
        }

        public static bool editStudent(int ppl_id, int crd_id)
        {
            librDataSetTableAdapters.studentInfoTableAdapter adapter = new librDataSetTableAdapters.studentInfoTableAdapter();
            librDataSet.studentInfoDataTable table = new librDataSet.studentInfoDataTable();
            adapter.Fill(table, crd_id, ppl_id);
            if (table.Rows.Count == 1)
            {
                string card_num = (string)table.Rows[0]["card_num"];
                DateTime navi_date = (DateTime)table.Rows[0]["navi_date"];
                DateTime upd_date = (DateTime)table.Rows[0]["upd_date"];
                DateTime end_date = (DateTime)table.Rows[0]["end_date"];
                string first_name = (string)table.Rows[0]["first_name"];
                string middle_name = (string)table.Rows[0]["middle_name"];
                string last_name = (string)table.Rows[0]["last_name"];
                string address = table.Rows[0]["address"].ToString();
                string phone = table.Rows[0]["phone"].ToString();
                string gender = (string)table.Rows[0]["gender"];
                DateTime birthDate = (DateTime)table.Rows[0]["birth_date"];

                frmEditStudent ns = new frmEditStudent();
                ns.Text = "Изменение карточки";
                ns.gbCard.Enabled = false;
                ns.tbCardNumber.Text = card_num;
                ns.dpEndDate.Value = end_date;
                ns.tbFirstName.Text = first_name;
                ns.tbMiddleName.Text = middle_name;
                ns.tbLastName.Text = last_name;
                ns.tbAddress.Text = address;
                ns.tbPhone.Text = phone;
                ns.cbGender.Text = gender;
                ns.dpBirthDate.Value = birthDate;
                if (ns.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    first_name = ns.tbFirstName.Text;
                    middle_name = ns.tbMiddleName.Text;
                    last_name = ns.tbLastName.Text;
                    birthDate = ns.dpBirthDate.Value;
                    gender = ns.cbGender.Text;
                    address = ns.tbAddress.Text;
                    phone = ns.tbPhone.Text;
                    librDataSetTableAdapters.peoplesTableAdapter adapterUpdate = new librDataSetTableAdapters.peoplesTableAdapter();
                    adapterUpdate.Update(1, first_name, middle_name, last_name, birthDate, gender, address, phone, ppl_id);
                    return true;
                    //setInfo();
                }
                else
                    return false;
            }
            else
                return false;

        }

        /// <summary>
        /// Открыть карточку читателя
        /// </summary>
        /// <param name="ppl_id">ID читателя</param>
        /// <param name="crd_id">ID карточки</param>
        /// <param name="pnl">В какую панель отобразить</param>
        public static void openStudent(int ppl_id, int crd_id)
        {
            frmStudent fs = new frmStudent(ppl_id, crd_id);
            fs.Show(Program._dp, DockState.Document);
        }

        public static void addBook()
        {
            frmEditBook eb = new frmEditBook();
            eb.btnOut.Visible = false;
            if (eb.ShowDialog() == DialogResult.OK)
            {
                string title = eb.tbTitle.Text;
                int pub_id = (int)eb.tbPublisher.Tag;
                string isbn = eb.tbISBN.Text;
                DateTime bookYear = DateTime.Parse(string.Format("01.01.{0}", eb.numYear.Value));
                int pages = (int)eb.numPages.Value;
                string bbk = eb.tbBBK.Text;
                string a_znak = eb.tbA_Znak.Text;
                int vol = (int)eb.numVol.Value;
                int ver = (int)eb.numVer.Value;
                int typ_id = (int)eb.tbTyp.Tag;
                string annotation = eb.tbAnnotation.Text;

                librDataSetTableAdapters.booksTableAdapter adapter = new librDataSetTableAdapters.booksTableAdapter();
                int new_book_id = adapter.InsertAndGetID(title, pub_id, isbn, bookYear, pages, bbk, a_znak, vol, ver, 0, typ_id, annotation);
                List<memberItem> members = eb.ucMemberEditor1.items;
                librDataSetTableAdapters.membersTableAdapter adapter_members = new librDataSetTableAdapters.membersTableAdapter();
                foreach (memberItem mi in members)
                {
                    adapter_members.Insert(new_book_id, mi._ppl_ppl_id, mi._mtyp_mtyp_id);
                }
                //if (MessageBox.Show("Книга добавлена. Перейти к экземплярам?", "Готово", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                //{
                //    MessageBox.Show(new_book_id.ToString());
                //}
            }
        }

        public static bool editBook(int book_id, bool isout=false)
        {
            frmEditBook eb = new frmEditBook(book_id, isout);
            eb.Text = "Редактирование книги";
            librDataSetTableAdapters.bookInfoTableAdapter adapter = new librDataSetTableAdapters.bookInfoTableAdapter();
            librDataSet.bookInfoDataTable table = new librDataSet.bookInfoDataTable();
            adapter.Fill(table, book_id);
            if (table.Rows.Count == 1)
            {
                eb.tbTitle.Text = table.Rows[0]["title"].ToString();
                eb.tbISBN.Text = table.Rows[0]["isbn"].ToString();
                librDataSetTableAdapters.publishersTableAdapter pub_adapter = new librDataSetTableAdapters.publishersTableAdapter();
                eb.tbPublisher.Text = pub_adapter.getName((int)table.Rows[0]["pub_pub_id"]);
                eb.tbPublisher.Tag = (int)table.Rows[0]["pub_pub_id"];
                eb.tbBBK.Text = table.Rows[0]["bbk"].ToString();
                eb.tbA_Znak.Text = table.Rows[0]["a_znak"].ToString();
                eb.numYear.Value = ((DateTime)table.Rows[0]["book_year"]).Year;
                eb.numPages.Value = (int)table.Rows[0]["pages"];
                eb.numVol.Value = (int)table.Rows[0]["vol"];
                eb.numVer.Value = (int)table.Rows[0]["ver"];
                librDataSetTableAdapters.book_typesTableAdapter typ_adapter = new librDataSetTableAdapters.book_typesTableAdapter();
                eb.tbTyp.Text = typ_adapter.getName((int)table.Rows[0]["typ_typ_id"]);
                eb.tbTyp.Tag = (int)table.Rows[0]["typ_typ_id"];
                eb.tbAnnotation.Text = table.Rows[0]["annotation"].ToString();
                eb.ucMemberEditor1.load();
                eb.ucExemplarsEditor1.load();
                if (eb.ShowDialog() == DialogResult.OK)
                {
                    string title = eb.tbTitle.Text;
                    int pub_id = (int)eb.tbPublisher.Tag;
                    string isbn = eb.tbISBN.Text;
                    DateTime bookYear = DateTime.Parse(string.Format("01.01.{0}", eb.numYear.Value));
                    int pages = (int)eb.numPages.Value;
                    string bbk = eb.tbBBK.Text;
                    string a_znak = eb.tbA_Znak.Text;
                    int vol = (int)eb.numVol.Value;
                    int ver = (int)eb.numVer.Value;
                    int typ_id = (int)eb.tbTyp.Tag;
                    string annotation = eb.tbAnnotation.Text;
                    librDataSetTableAdapters.booksTableAdapter upd_adapter = new librDataSetTableAdapters.booksTableAdapter();
                    upd_adapter.Update(title, pub_id, isbn, bookYear, pages, bbk, a_znak, vol, ver, 0, typ_id, annotation, book_id);
                    exmplr_id = eb.exmplr_id;
                    return true;
                }
                else
                    return true;
            }
            return
                false;
        }

        public static void bookOut(int? ppl_id = null, int? crd_id = null, List<int> exmplr_ids = null)
        {
            int nppl_id = ppl_id == null ? -1 : (int)ppl_id;
            int ncrd_id = crd_id == null ? -1 : (int)crd_id;
            List<int> ex = new List<int>();
            if (exmplr_ids != null)
                ex = exmplr_ids;

            frmBookOut bo = new frmBookOut(nppl_id, ncrd_id, ex);
            bo.Show(Program._dp, DockState.Document);
        }

        public static void bookIn(int? ppl_id = null, int? crd_id = null, List<int> exmplr_ids = null)
        {
            int nppl_id = ppl_id == null ? -1 : (int)ppl_id;
            int ncrd_id = crd_id == null ? -1 : (int)crd_id;
            List<int> ex = new List<int>();
            if (exmplr_ids != null)
                ex = exmplr_ids;

            frmBookIn bi = new frmBookIn(nppl_id, ncrd_id, ex);
            bi.Show(Program._dp, DockState.Document);
        }

        public static string LibrarianName()
        {
            librDataSetTableAdapters.peoples_membersTableAdapter adapter = new librDataSetTableAdapters.peoples_membersTableAdapter();
            return adapter.getName(Program.Librarian);
        }

        public static void delBook(int book_id)
        {
            if (book_id != -1)
            {
                librDataSetTableAdapters.deliveriesTableAdapter adapter1 = new librDataSetTableAdapters.deliveriesTableAdapter();
                librDataSetTableAdapters.exemplarsTableAdapter adapter2 = new librDataSetTableAdapters.exemplarsTableAdapter();
                librDataSetTableAdapters.booksTableAdapter adapter3 = new librDataSetTableAdapters.booksTableAdapter();
                adapter1.DeleteByBookID(book_id);
                adapter2.DeleteByBookID(book_id);
                adapter3.Delete(book_id);
            }
        }
    }
}
