﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.Data.OleDb;

namespace library
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void memberTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMemberTypes mt = new frmMemberTypes();
            mt.pnlButtons.Visible = false;
            mt.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void publishersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPublishers pub = new frmPublishers();
            pub.pnlButtons.Visible = false;
            pub.Show();
        }

        private void cardStatusesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCardStatuses cs = new frmCardStatuses();
            cs.pnlButtons.Visible = false;
            cs.Show();
        }

        private void bookVidsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBookVids bv = new frmBookVids();
            bv.pnlButtons.Visible = false;
            bv.Show();
        }

        private void bookTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBookTypes bt = new frmBookTypes();
            bt.pnlButtons.Visible = false;
            bt.Show();
        }

        private void newStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            api.addStudent();
        }

        private void findStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFindStudent fs = new frmFindStudent();
            if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                int ppl_id = fs.ppl_id;
                int crd_id = fs.crd_id;
                if (ppl_id != -1 && crd_id != -1)
                    api.openStudent(ppl_id, crd_id);
            }
        }

        private void cardServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFindStudent fs = new frmFindStudent();
            if (fs.ShowDialog() == DialogResult.OK)
            {
                int ppl_id = fs.ppl_id;
                int crd_id = fs.crd_id;
                frmChangeCard cc = new frmChangeCard();
                cc.tbOldCardNumber.Text = fs.cardNum;
                if (cc.ShowDialog() == DialogResult.OK)
                {
                    int new_crd_id = api.changeCard(ppl_id, crd_id, cc.tbCardNumber.Text, cc.dpEndDate.Value);
                    if (MessageBox.Show("Замена произведена, открыть карточку?", "Замена", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        api.openStudent(ppl_id, new_crd_id);
                    }
                }
            }
        }

        private void newBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            api.addBook();
        }

        private void findBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFindBook fb = new frmFindBook();
            if (fb.ShowDialog() == DialogResult.OK)
            {
                int book_id = fb.book_id;
                if (book_id != -1)
                    api.editBook(book_id);
            }
        }

        private void bookOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            api.bookOut();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Program._dp = dockPnl;
            frmLogon l = new frmLogon();
            if (l.ShowDialog() != DialogResult.OK)
                this.Close();
            this.Text = api.LibrarianName();
            //api.openStudent(4, 3);
            //api.bookIn(4, 3);
        }

        private void bookInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            api.bookIn();
        }

        private void changeLibrarianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLogon l = new frmLogon();
            if (l.ShowDialog() == DialogResult.OK)
                this.Text = api.LibrarianName();
        }

        private void librariansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLibrarians lib = new frmLibrarians();
            lib.pnlButtons.Visible = false;
            lib.Show();

        }

        private void delBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFindBook fb = new frmFindBook();
            if (fb.ShowDialog() == DialogResult.OK && fb.book_id != -1)
                if (MessageBox.Show("Вы действительно хотите удалить всю информацию о данной книге? (включая всю историю выдачи на руки)", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    api.delBook(fb.book_id);
        }

        private void repUsedBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBookReport br = new frmBookReport(frmBookReport.reportType.allBooks);
            br.Show(Program._dp, DockState.Document);
        }

        private void repOutOfDateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBookReport br = new frmBookReport(frmBookReport.reportType.expiredBooks);
            br.Show(Program._dp, DockState.Document);
        }

    }
}
